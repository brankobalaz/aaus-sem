﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Tree23;

namespace Tree23Tester
{
    class Program
    {
        public static Timer _timer1;
        public static int _i;
        public static int _numberOfOperationsInTest = 1000000;
        public static int _minNum = 0;
        public static int _maxNum = Int32.MaxValue;
        public static int _head = 0;
        public static char[] _headChar = {'|', '/', '-', '\\'};

        static void Main(string[] args)
        {
            var tree = new Tree23<int>();
            var list = new List<int>();

            Console.WriteLine("Test 1, operations Insert and Remove 50:50 :");

            _timer1 = new Timer(WriteProgressBar, null, 0, 100);

            TestInsertRemove(tree, list, 0.5);

            WriteProgressBar(null);

            Console.WriteLine($"\nTest result is: {(IsTreeOK(tree, list) ? "OK" : "NG")}");

            tree = new Tree23<int>();
            list = new List<int>();

            Console.WriteLine("\nTest 2, operations Insert and Remove 80:20 :");

            TestInsertRemove(tree, list, 0.8);
            
            WriteProgressBar(null);

            Console.WriteLine($"\nTest result is: {(IsTreeOK(tree, list) ? "OK" : "NG")}");

            _numberOfOperationsInTest = 500;

            Console.WriteLine($"\nTest 3, Interval search on {_numberOfOperationsInTest} random intervals :");
            var test3Result = TestIntervalSearch(tree, list, _numberOfOperationsInTest);
            
            WriteProgressBar(null);
            _timer1.Dispose();

            Console.WriteLine($"\nTest result is: {(test3Result ? "OK" : "NG")}");

            Console.WriteLine("\nTest 4, every leaf is in same level of tree");

            Console.WriteLine($"Test result is: {(tree.TestBalance() ? "OK" : "NG")}");
            Console.WriteLine();

            Console.WriteLine("\nTest 5, clear");

            var removedCount = tree.Clear();

            Console.WriteLine($"{removedCount} items was removed.");
            Console.WriteLine($"Test result is: {(tree.Size == 0 ? "OK" : "NG")}");
            Console.WriteLine();

        }

        private static bool TestIntervalSearch(Tree23<int> tree, List<int> list, int countOfTests)
        {
            var rand = new Random();
            var isOK = true;

            for (_i = 0; _i < countOfTests; _i++)
            {
                var min = rand.Next(_maxNum / 2);
                var max = rand.Next(_maxNum / 2 + 1, _maxNum);

                var correctInterval = list.Where(x => min <= x && x <= max).ToList();
                var treeInterval = tree.GetIntervalEnumerator(min, max).ToList();
                var iterationOK = correctInterval.SequenceEqual(treeInterval);

                isOK &= iterationOK;
            }

            return isOK;
        }

        private static void TestInsertRemove(Tree23<int> tree, List<int> list, double insertProbability)
        {
            var rand = new Random();

            for (_i = 0; _i < _numberOfOperationsInTest; _i++)
            {
                var r = rand.NextDouble();

                if (r < insertProbability) // INSERT
                {
                    var randomNumber = rand.Next(_maxNum);

                    if (tree.Contains(randomNumber))
                        continue;

                    tree.Insert(randomNumber);
                    list.Add(randomNumber);
                }
                else
                {
                    if (tree.Size != 0)
                    {
                        var randomNumber = rand.Next(tree.Size);

                        tree.Remove(list[randomNumber]);
                        list.RemoveAt(randomNumber);
                    }
                }
            }
        }

        private static bool IsTreeOK(Tree23<int> tree, List<int> list)
        {
            if (tree.Size != list.Count)
                return false;

            list.Sort();
            int o = 0;
            foreach (var i in tree)
            {
                if (list[o] != i)
                    return false;
                o++;
            }

            return true;
        }

        private static void WriteProgressBar(Object o)
        {
            var length = 30;
            var done = _i / (_numberOfOperationsInTest / length);

            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("[");
            for (var i = 0; i < length; ++i)
            {
                if (i < done)
                    Console.Write("=");
                else if (i == done)
                    Console.Write(_headChar[_head]);
                else
                    Console.Write(" ");
            }

            Console.Write($"] ({(_i / (double) _numberOfOperationsInTest * 100).ToString("#.00")}%)  ");

            _head = ++_head % _headChar.Length;
        }
    }
}