﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace BTree3Tester
{
    public class Property : IRecord, IComparable<Property>, IComparable
    {
        private const int MAX_NAME_SIZE = 20;
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public double DecimalValue { get; set; }

        public override string ToString()
        {
            return $"{Id}";
        }

        public byte[] ToByteArray()
        {
            var idBytes = BitConverter.GetBytes(Id);
            var nameValidBytes = BitConverter.GetBytes((Name ?? "").Length);
            var tempName = (Name ?? "").PadRight(MAX_NAME_SIZE, '*');
            var nameBytes = Name == null
                ? new byte[MAX_NAME_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempName.ToCharArray());
            var birthdayBytes = BitConverter.GetBytes(Birthday.Ticks);
            var decimaValueBytes = BitConverter.GetBytes(DecimalValue);

            return idBytes
                .Concat(nameValidBytes)
                .Concat(nameBytes)
                .Concat(birthdayBytes)
                .Concat(decimaValueBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var idBytes = array[..4];
            var nameValidBytes = array[4..8];
            var nameBytes = array[8..(2 * MAX_NAME_SIZE + 8)];
            var birthdayBytes = array[(2 * MAX_NAME_SIZE + 8)..(2 * MAX_NAME_SIZE + 8 + sizeof(long))];
            var decimalValueBytes = array[(2 * MAX_NAME_SIZE + 8 + sizeof(long))..];

            Id = BitConverter.ToInt32(idBytes);
            var nameValidLength = BitConverter.ToInt32(nameValidBytes);
            Name = Encoding.Unicode.GetString(nameBytes[..(2 * nameValidLength)]);
            if (Name == "") Name = null;
            var ticks = BitConverter.ToInt64(birthdayBytes);
            Birthday = DateTime.FromBinary(ticks);
            DecimalValue = BitConverter.ToDouble(decimalValueBytes);
        }

        public int ByteSize()
        {
            return 2 * sizeof(int) + MAX_NAME_SIZE * sizeof(char) + sizeof(long) + sizeof(double);
        }

        public int CompareTo(Property other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }

        public int CompareTo(object obj)
        {
            if (ReferenceEquals(null, obj)) return 1;
            if (ReferenceEquals(this, obj)) return 0;
            return obj is Property other
                ? CompareTo(other)
                : throw new ArgumentException($"Object must be of type {nameof(Property)}");
        }
    }
}