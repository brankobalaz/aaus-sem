﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Bogus;
using BTree3;

namespace BTree3Tester
{
    class Program
    {
        private static bool CLEAR = false;

        public static int _numberOfOperationsInTest = 500;
        public static int _maxNum = Int32.MaxValue;

        static void Main(string[] args)
        {
            using var tree = new BTree3<Property>("./data/xbtree.data");
            var list = new List<Property>();

            //tree.Insert(new Property {Id = 10, Name = "Braňo", Birthday = DateTime.Parse("21.02.2000"), DecimalValue = 42.2});
            //Property a = tree.Find(new Property {Id = 10}, out var found10);
            //Console.WriteLine( found10 ? a.Name : "Nenašiel sa");
            
            //return;

            //var i = 0;
            //foreach (var t in tree)
            //{
            //    tree.Remove(t);
            //    i++;
            //    if(i == 10) break;
            //}

            //var fileContent = tree.GetFileContent();

            //foreach (var file in fileContent)
            //{
            //    Console.WriteLine(file);
            //}
            
            Console.WriteLine($"Tree count: {tree.Count}");

            if (tree.Count != 0)
                foreach (var data in tree)
                {
                    list.Add(data);
                }

            Console.WriteLine($"List count: {list.Count}");

            Console.WriteLine("\nTest 1, operations Insert and Remove 50:50 :");
            TestInsertRemove(tree, list, 0.5);
            Console.WriteLine($"\nTest result is: {(IsTreeOK(tree, list) ? "OK" : "NG")}");

            Console.WriteLine("\nTest 2, operations Insert and Remove 80:20 :");
            TestInsertRemove(tree, list, 0.8);
            Console.WriteLine($"\nTest result is: {(IsTreeOK(tree, list) ? "OK" : "NG")}");

            _numberOfOperationsInTest = 100;

            Console.WriteLine($"\nTest 3, Interval search on {_numberOfOperationsInTest} random intervals :");
            var test3Result = TestIntervalSearch(tree, list, _numberOfOperationsInTest);
            Console.WriteLine($"\nTest result is: {(test3Result ? "OK" : "NG")}");

            Console.WriteLine("\nTest 4, every leaf is in same level of tree");
            Console.WriteLine($"Test result is: {(tree.TestBalance() ? "OK" : "NG")}");

            if (CLEAR)
            {
                Console.WriteLine("\nTest 5, clear");
                Console.WriteLine($"{tree.Size} = tree count before clear.");
                var removedCount = tree.Clear();
                Console.WriteLine($"{removedCount} items was removed.");
                Console.WriteLine($"Test result is: {(tree.Size == 0 ? "OK" : "NG")}");
                Console.WriteLine();
            }

            var fileContent = tree.GetFileContent();

            foreach (var file in fileContent)
            {
                Console.WriteLine(file);
            }
        }

        private static bool TestIntervalSearch(BTree3<Property> tree, List<Property> list, int countOfTests)
        {
            var rand = new Random();
            var faker = new Faker("sk");

            for (var i = 0; i < countOfTests; i++)
            {
                var idMin = rand.Next(_maxNum / 2);
                var propMin = new Property {Id = idMin};
                
                var idMax = rand.Next(_maxNum / 2 + 1, _maxNum);
                var propMax = new Property {Id = idMax};

                var correctInterval = list.Where(x => propMin.CompareTo(x) <= 0 && x.CompareTo(propMax) <= 0).ToList();
                var treeInterval = tree.GetIntervalEnumerator(propMin, propMax).ToList();

                if (correctInterval.Count != treeInterval.Count)
                    return false;

                var resultCount = correctInterval.Count;

                for (var j = 0; j < resultCount; j++)
                {
                    if (correctInterval[j].CompareTo(treeInterval[j]) != 0)
                    {
                        Console.WriteLine(correctInterval[j]);
                        Console.WriteLine(treeInterval[j]);
                        return false;
                    }
                }

                if ((i + 1) % 5 == 0)
                    WriteProgressBar(i, _numberOfOperationsInTest);
            }

            return true;
        }

        private static void TestInsertRemove(BTree3<Property> tree, List<Property> list, double insertProbability)
        {
            var rand = new Random();
            var faker = new Faker("sk");

            for (var i = 0; i < _numberOfOperationsInTest; i++)
            {
                var r = rand.NextDouble();

                if (r < insertProbability || tree.Count == 0) // INSERT
                {
                    var name = faker.Name.FirstName();
                    var date = faker.Date.Past(30);
                    var id = rand.Next();
                    var prop = new Property {Id = id, Name = "Maria", Birthday = DateTime.Parse("21.02.2000")};

                    if (tree.Insert(prop))
                        list.Add(prop);
                }
                else
                {
                    var randomNumber = rand.Next(list.Count);
                    var randomKey = list[randomNumber];

                    tree.Remove(randomKey);
                    list.RemoveAt(randomNumber);
                }

                if ((i + 1) % 1000 == 0)
                    WriteProgressBar(i, _numberOfOperationsInTest);
            }
        }

        private static bool IsTreeOK(BTree3<Property> tree, List<Property> list)
        {
            if (tree.Count != list.Count)
                return false;

            list.Sort();
            int o = 0;
            foreach (var i in tree)
            {
                if (list[o].CompareTo(i) != 0)
                    return false;

                o++;
            }

            return true;
        }

        private static void WriteProgressBar(int i, int max)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("[");
            var pivot = 20L * (i + 1) / max;
            for (int j = 0; j < 20; j++)
            {
                if (j < pivot)
                    Console.Write("*");
                else
                    Console.Write(" ");
            }

            Console.Write("]");
            Console.Out.Flush();
        }
    }
}