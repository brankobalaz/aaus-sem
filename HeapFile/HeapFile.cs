﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Priority_Queue;

namespace HeapFile
{
    public class HeapFile<T> : IDisposable, IEnumerable<T> where T : IRecord, new()
    {
        private const string DATA_EXT = ".data";
        private const string FREE_ADDRESSES_EXT = ".fa";

        private readonly SimplePriorityQueue<long> _freeAddresses = new();
        protected readonly FileStream _dataFileStream;
        private readonly string _freeAddressesFileName;
        protected readonly int _recordSize;
        protected bool WasSavedProperly { get; private set; }
        protected bool WasSaved { get; private set; }

        /// <summary>
        /// Create new HeapFile instance.
        /// </summary>
        /// <param name="dataFileName">Accepts file with .data extension.</param>
        public HeapFile(string dataFileName)
        {
            if (Path.GetExtension(dataFileName) != ".data")
                throw new ArgumentException("file must have .data extension!");

            _recordSize = new Record<T>().ByteSize();

            if (File.Exists(dataFileName))
                WasSaved = true;

            EnsureFolder(dataFileName);

            _dataFileStream = new FileStream(dataFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            _freeAddressesFileName = dataFileName.Substring(0, dataFileName.Length - 5) + ".fa";
            LoadFreeAddressesQueue(_freeAddressesFileName);
        }

        private void LoadFreeAddressesQueue(string freeAddFileName)
        {
            try
            {
                using (var freeAddressesFileStream = new FileStream(freeAddFileName, FileMode.Open, FileAccess.Read))
                {
                    WasSavedProperly = true;

                    var addr = 0;
                    while (addr < freeAddressesFileStream.Length)
                    {
                        freeAddressesFileStream.Seek(addr, SeekOrigin.Begin);
                        var numBytes = new byte[sizeof(long)];
                        freeAddressesFileStream.Read(numBytes, 0, sizeof(long));
                        var num = BitConverter.ToInt64(numBytes);
                        _freeAddresses.Enqueue(num, num);

                        addr += sizeof(long);
                    }
                }

                File.Delete(freeAddFileName);
            }
            catch (FileNotFoundException)
            {
                ReconstructFreeAddressesQueue();
            }
        }

        public long Insert(T data)
        {
            var address = GetFreeAddress();

            _dataFileStream.Seek(address, SeekOrigin.Begin);

            var record = new Record<T>(data, true);
            _dataFileStream.Write(record.ToByteArray());

            return address;
        }

        public bool UpdateAt(long address, T data)
        {
            if (IsAddressValid(address))
            {
                _dataFileStream.Seek(address, SeekOrigin.Begin);

                var record = new Record<T>(data, true);
                _dataFileStream.Write(record.ToByteArray());
                return true;
            }
            else
            {
                return false;
            }
        }

        private long GetFreeAddress()
        {
            return _freeAddresses.Count == 0 ? _dataFileStream.Length : _freeAddresses.Dequeue();
        }

        public bool RemoveAt(long address)
        {
            if (IsAddressValid(address))
            {
                RemoveAtUnsafe(address);
                return true;
            }

            return false;
        }

        public void RemoveAtUnsafe(long address)
        {
            _freeAddresses.EnqueueWithoutDuplicates(address, address);

            // Odstránil som dáta z poslednej adresy
            var fileLength = _dataFileStream.Length;
            if (address == fileLength - _recordSize)
            {
                TrimFile();
                return;
            }

            // Inak záznam prepíšem nevalidným záznamom (dáta sa prepíšu na disku)
            _dataFileStream.Seek(address, SeekOrigin.Begin);
            _dataFileStream.Write(new Record<T> {IsValid = false}.ToByteArray());
        }

        private void TrimFile()
        {
            var validLength = GetValidLength();

            // Upravím dostupné voľné adresy (Odstránim väčšie ako veľkosť súboru)
            var fileLength = _dataFileStream.Length;
            var addr = validLength;
            while (addr < fileLength)
            {
                _freeAddresses.Remove(addr);
                addr += _recordSize;
            }

            _dataFileStream.SetLength(validLength);
        }

        private long GetValidLength()
        {
            var fileLength = _dataFileStream.Length;
            var addr = fileLength - _recordSize;
            while (addr >= 0)
            {
                // Zastaví sa na prvom platnom prvku
                if (!_freeAddresses.Contains(addr))
                    break;

                addr -= _recordSize;
            }

            var validLength = addr + _recordSize;
            return validLength;
        }

        public T GetAtUnsafe(long address)
        {
            var record = GetRecordAt(address);
            return record.Data;
        }

        protected Record<T> GetRecordAt(long address)
        {
            _dataFileStream.Seek(address, SeekOrigin.Begin);
            var byteRecord = new byte[_recordSize];
            _dataFileStream.Read(byteRecord, 0, _recordSize);

            var record = new Record<T>();
            record.LoadFromByteArray(byteRecord);

            return record;
        }

        public T GetAt(long address)
        {
            if (IsAddressValid(address))
                return GetAtUnsafe(address);

            throw new IndexOutOfRangeException("Neplatná adresa!");
        }

        public T Find(T shellData)
        {
            var addr = 0;
            while (addr < _dataFileStream.Length)
            {
                if (IsAddressValid(addr))
                {
                    var data = GetAt(addr);

                    if (data.Equals(shellData))
                        return data;
                }

                addr += _recordSize;
            }

            return default;
        }

        private bool IsAddressValid(long address)
        {
            return 0 <= address
                   && address < _dataFileStream.Length
                   && !_freeAddresses.Contains(address)
                   && address % _recordSize == 0;
        }

        public void Clear()
        {
            _freeAddresses.Clear();
            _dataFileStream.SetLength(0);
        }

        public void SaveFreeAddressesToFile()
        {
            using var freeAddressesFileStream =
                new FileStream(_freeAddressesFileName, FileMode.OpenOrCreate, FileAccess.Write);

            freeAddressesFileStream.SetLength(0); // vždy sa po načítaní maže, takže toto by nemalo byť treba

            foreach (var freeAddress in _freeAddresses)
                freeAddressesFileStream.Write(BitConverter.GetBytes(freeAddress));
        }

        private void ReconstructFreeAddressesQueue()
        {
            var addr = 0;
            while (addr < _dataFileStream.Length)
            {
                var record = GetRecordAt(addr);

                if (!record.IsValid)
                    _freeAddresses.Enqueue(addr, addr);

                addr += _recordSize;
            }
        }

        // https://stackoverflow.com/questions/3695163/filestream-and-creating-folders
        private void EnsureFolder(string path)
        {
            string directoryName = Path.GetDirectoryName(path);
            // If path is a file name only, directory name will be an empty string
            if (directoryName?.Length > 0)
            {
                // Create all directories on the path that don't already exist
                Directory.CreateDirectory(directoryName);
            }
        }
        public LinkedList<string> GetFileContent()
        {
            var res = new LinkedList<string>();

            string line;
            var addr = 0;
            while (addr < _dataFileStream.Length)
            {
                var record = GetRecordAt(addr);

                line = "";
                line += addr.ToString().PadLeft(10) + ": ";
                line += (record.IsValid ? "VALID" : "INVAL") + " ";
                line += record.Data.ToString();

                if (record.Data is IDisposable)
                    ((IDisposable)record.Data).Dispose();

                res.AddLast(line);

                addr += _recordSize;
            }

            return res;
        }

        public virtual void Dispose()
        {
            _dataFileStream?.Dispose();
            SaveFreeAddressesToFile();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var addr = 0;
            while (addr < _dataFileStream.Length)
            {
                var record = GetRecordAt(addr);

                yield return record.Data;

                addr += _recordSize;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}