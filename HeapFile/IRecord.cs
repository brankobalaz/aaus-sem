﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapFile
{
    public interface IRecord
    {
        public byte[] ToByteArray();
        public void LoadFromByteArray(byte[] array);
        public int ByteSize();
    }
}
