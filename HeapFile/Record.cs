﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapFile
{
    public class Record<T> : IRecord where T : IRecord, new()
    {
        private int _dataSize;
        public T Data { get; set; }
        public bool IsValid { get; set; }

        public Record()
        {
            _dataSize = new T().ByteSize();
        }

        public Record(T data, bool isValid)
        {
            Data = data;
            IsValid = isValid;
            _dataSize = data.ByteSize();
        }

        public byte[] ToByteArray()
        {
            var isValidBytes = BitConverter.GetBytes(IsValid);
            var dataBytes = Data == null ? new byte[_dataSize] : Data.ToByteArray();
            return isValidBytes
                .Concat(dataBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var validBytes = array[..sizeof(bool)];
            var dataBytes = array[sizeof(bool)..];

            IsValid = BitConverter.ToBoolean(validBytes);
            Data = new T();
            Data.LoadFromByteArray(dataBytes);
        }

        public int ByteSize()
        {
            return sizeof(bool) + _dataSize;
        }
    }
}