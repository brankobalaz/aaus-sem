﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using Caliburn.Micro;
using PCRTestsEvidence.Models;
using PCRTestsEvidence.ViewModels;

namespace PCRTestsEvidence
{
    public class Bootstrapper : BootstrapperBase
    {
        private SimpleContainer _container;

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _container = new SimpleContainer();

            _container.Instance(_container);

            _container
                .Singleton<IWindowManager, WindowManager>()
                .Singleton<Evidence>()
                .Singleton<Generator>()
                .Singleton<DataLoader>();

            _container
                .PerRequest<ShellViewModel>()
                .PerRequest<AddPatientViewModel>()
                .PerRequest<AddTestViewModel>()
                .PerRequest<ShowTestsRemovePatientViewModel>()
                .PerRequest<ShowTestsByDistrictViewModel>()
                .PerRequest<ShowTestsByRegionViewModel>()
                .PerRequest<ShowTestsByWorkplaceViewModel>()
                .PerRequest<ShowOrRemoveTestViewModel>()
                .PerRequest<GeneratorViewModel>()
                .PerRequest<ShowTestsByDateViewModel>()
                .PerRequest<ShowSickPatientsInDistrictViewModel>()
                .PerRequest<ShowSickPatientsInRegionViewModel>()
                .PerRequest<ShowDistrictsByPositivityViewModel>()
                .PerRequest<ShowRegionsByPositivityViewModel>()
                .PerRequest<ShowSickPatientsInEvidenceViewModel>()
                .PerRequest<ShowFileContentViewModel>();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("sk-SK");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("sk-SK");

            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            IoC.Get<Evidence>().Dispose();
            base.OnExit(sender, e);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }
}