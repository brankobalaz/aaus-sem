﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRTestsEvidence.Models
{
    public class DataLoader
    {
        private readonly Evidence _evidence;

        public DataLoader(Evidence evidence)
        {
            _evidence = evidence;
        }

        public async Task<int> LoadTests(string filePath)
        {
            using var file = new StreamReader(File.OpenRead(filePath));

            // Ignorácia hlavičky
            if (!file.EndOfStream)
                await file.ReadLineAsync();

            var testsAdded = 0;

            while (!file.EndOfStream)
            {
                var line = await file.ReadLineAsync();
                var splitted = line.Split(';');

                // Ak by sa nepodatilo sparsovať niečo, tak budem test ignorovať
                try
                {
                    var res = _evidence.AddTest(Guid.Parse(splitted[0]), DateTime.Parse(splitted[1]), splitted[2],
                        int.Parse(splitted[3]), int.Parse(splitted[4]), int.Parse(splitted[5]),
                        splitted[6].Equals("1"), splitted[7]);

                    if (res == Evidence.ERR_NO_ERROR)
                        testsAdded++;
                }
                catch
                {
                }
            }

            return testsAdded;
        }

        public async Task<int> LoadPatients(string filePath)
        {
            using var file = new StreamReader(File.OpenRead(filePath));

            // Ignorácia hlavičky
            if (!file.EndOfStream)
                await file.ReadLineAsync();

            var patientsAdded = 0;

            while (!file.EndOfStream)
            {
                var line = await file.ReadLineAsync();
                var splitted = line.Split(';');

                // Ak by sa dátum nepodarilo sparsovať, budem záznam ignorovať
                try
                {
                    if (_evidence.AddPatient(splitted[1], splitted[2], DateTime.Parse(splitted[3]), splitted[0]))
                        patientsAdded++;
                }
                catch
                {
                }
            }

            return patientsAdded;
        }

        public async Task<int> SaveTests(string filePath)
        {
            await using var file = new StreamWriter(filePath, append: true);
            await file.WriteLineAsync("testId;timestamp;patientId;workplaceId;districtId;regionId;result;note");

            var tests = _evidence.GetAllShellTests();

            foreach (var test in tests)
                await file.WriteLineAsync(
                    $"{test.TestId};{test.TimeStamp};{test.Patient.Id};{test.WorkplaceId};{test.DistrictId};{test.RegionId};{(test.Result ? '1' : '0')};{test.Note}");

            return tests.Count;
        }

        public async Task<int> SavePatients(string filePath)
        {
            await using var file = new StreamWriter(filePath, append: true);
            await file.WriteLineAsync("patientId;name;surname;birthday");

            var patients = _evidence.GetAllShellPatients();

            foreach (var patient in patients)
                await file.WriteLineAsync($"{patient.Id};{patient.Name};{patient.Surname};{patient.Birthday}");

            return patients.Count;
        }
    }
}