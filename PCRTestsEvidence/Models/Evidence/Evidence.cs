﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using HeapFile;
using BTree3;

namespace PCRTestsEvidence.Models
{
    public class Evidence : IDisposable
    {
        public const int ERR_NO_ERROR = 0;
        public const int ERR_OBJECT_NOT_FOUND = -1;

        public const int GET_ALL_TESTS = 2;
        public const int GET_POSITIVE_TESTS = 3;

        public const string NOT_FOUND = "";

        private HeapFile<PCRTest> _heapTests;

        private BTree3<Patient> _patients;

        private BTree3<Workplace> _workplaces;
        private BTree3<District> _districts;
        private BTree3<Region> _regions;

        private BTree3<PCRTestByIdAddr> _allTestsByIds;
        private BTree3<PCRTestByDateAddr> _allTestsByDate;
        private BTree3<PCRTestByDateAddr> _positiveTestsByDate;

        private readonly Guid _guidMin = new Guid(new byte[16]);

        private readonly Guid _guidMax = new Guid(new byte[]
            {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});

        public Evidence()
        {
            _heapTests = new HeapFile<PCRTest>("./data/tests_heap/data.data");

            _patients = new BTree3<Patient>("./data/patients_by_id/patients_by_id.data");

            _workplaces = new BTree3<Workplace>("./data/workplaces_by_id/workplaces.data");
            _districts = new BTree3<District>("./data/districts_by_id/districts.data");
            _regions = new BTree3<Region>("./data/regions_by_id/regions.data");

            _allTestsByIds = new BTree3<PCRTestByIdAddr>("./data/tests_by_id/tests_by_id.data");
            _allTestsByDate = new BTree3<PCRTestByDateAddr>("./data/tests_by_date/tests_by_date.data");
            _positiveTestsByDate = new BTree3<PCRTestByDateAddr>("./data/tests_positive/tests_positive.data");
        }

        private PCRTest GetTest(string stringGuid, out bool found)
        {
            try
            {
                if (Guid.TryParse(stringGuid, out var guid))
                    return GetTest(guid, out found);
            }
            catch
            {
            }

            found = false;
            return default;
        }

        private PCRTest GetTest(Guid testGuid, out bool found)
        {
            var tempTest = new PCRTestByIdAddr(testGuid);
            var testById = _allTestsByIds.Find(tempTest, out found);

            if (!found)
                return default;

            var testAddr = testById.PCRTestAddr;
            var test = GetTestAt(testAddr);

            return test;
        }

        private PCRTest GetTestAt(long address)
        {
            return _heapTests.GetAt(address);
        }

        private long InsertTest(PCRTest test)
        {
            return _heapTests.Insert(test);
        }

        private bool RemoveTestAt(long addr)
        {
            return _heapTests.RemoveAt(addr);
        }

        public ShellPCRTest GetShellTest(string stringGuid, out bool found)
        {
            var test = GetTest(stringGuid, out found);

            if (!found)
                return default;

            var patient = GetPatient(test.PatientId, out _);

            return new ShellPCRTest(test, patient);
        }

        public ShellPatient GetShellPatient(string id, out bool found)
        {
            var patient = GetPatient(id, out found);

            if (!found)
                return default;

            return new ShellPatient(patient);
        }

        private bool AddPatient(Patient patient)
        {
            return _patients.Insert(patient);
        }

        public bool AddPatient(string name, string surname, DateTime birthDate, string id = "")
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                var rand = new Random();

                var newId = (birthDate.Year % 100).ToString("D2");
                newId += (birthDate.Month + (rand.NextDouble() < 0.512 ? 50 : 0)).ToString("D2");
                newId += birthDate.Day.ToString("D2");
                newId += rand.Next(1000).ToString("D3");
                var diff = int.Parse(newId) % 11;
                diff = diff == 10 ? 0 : diff;
                newId += diff.ToString();

                id = newId;
            }

            var patient = new Patient(name, surname, birthDate, id);
            return AddPatient(patient);
        }

        public int RemovePatient(string patientId)
        {
            var patient = GetPatient(patientId, out var found);

            if (!found)
                return ERR_OBJECT_NOT_FOUND;

            _patients.Remove(patient);
            var testRemoved = RemoveTestsOfPatient(patient);
            patient.Clear();

            return testRemoved;
        }

        // Vráti počet testov, ktoré sa odstránili
        private int RemoveTestsOfPatient(Patient patient)
        {
            var testsAddresses = patient.GetAllTestsAddr();
            var testsCount = testsAddresses.Count();

            foreach (var testAddr in testsAddresses)
            {
                var test = GetTestAt(testAddr);
                RemoveTestPartialNonSafe(test);
                RemoveTestAt(testAddr);
            }

            return testsCount;
        }

        // Neodstraňuje z pacienta a nekontroluje existenciu testu!!!
        public int RemoveTestPartialNonSafe(PCRTest pcrTest)
        {
            var testById = new PCRTestByIdAddr(pcrTest.TestId);
            var testByDate = new PCRTestByDateAddr(pcrTest);
            var positivity = pcrTest.Result;

            // Tests
            _allTestsByIds.Remove(testById);

            _allTestsByDate.Remove(testByDate);

            if (positivity == PCRTest.POSITIVE)
                _positiveTestsByDate.Remove(testByDate);

            // District
            var district = GetDistrict(pcrTest.DistrictId);
            district.RemoveTestByDate(testByDate, positivity);

            // Region
            var region = GetRegion(pcrTest.RegionId);
            region.RemoveTestByDate(testByDate, positivity);

            // Workplace
            var workplace = GetWorkplace(pcrTest.WorkplaceId);
            workplace.RemoveTestByDate(testByDate);

            return ERR_NO_ERROR;
        }

        public void ClearEvidence(out int patientsCount, out int testsCount)
        {
            patientsCount = _patients.Size;
            testsCount = _allTestsByIds.Size;
            _heapTests.Clear();

            foreach (var workplaceById in _workplaces)
            {
                var workplace = new Workplace(workplaceById.Id);
                workplace.Clear();
            }

            _workplaces.Clear();

            foreach (var districtById in _districts)
            {
                var district = new District(districtById.Id);
                district.Clear();
            }

            _districts.Clear();

            foreach (var regionById in _regions)
            {
                var region = new Region(regionById.Id);
                region.Clear();
            }

            _regions.Clear();

            _patients.Clear();
            _allTestsByIds.Clear();
            _allTestsByDate.Clear();
            _positiveTestsByDate.Clear();
        }

        public List<ShellPatient> GetAllShellPatients()
        {
            return _patients.Select(x => new ShellPatient(x)).ToList();
        }

        public Patient GetPatient(string patientId, out bool found)
        {
            var tempPatient = new Patient(patientId);
            var patient = _patients.Find(tempPatient, out found);

            if (!found)
                return default;

            return patient;
        }

        // Prijme test s dočasne vytvoreným pacientom (pacient má adresu NOT_FOUND)
        private int AddTest(PCRTest pcrTest)
        {
            var patient = GetPatient(pcrTest.PatientId, out var found);

            if (!found)
                return ERR_OBJECT_NOT_FOUND;

            var addr = InsertTest(pcrTest);

            var testById = new PCRTestByIdAddr(pcrTest.TestId, addr);
            var testByDate = new PCRTestByDateAddr(pcrTest, addr);
            var positivity = pcrTest.Result;

            // Inserting to structures:

            // Patient
            patient.AddTestByDate(testByDate);

            // Tests
            _allTestsByIds.Insert(testById);

            _allTestsByDate.Insert(testByDate);

            if (positivity == PCRTest.POSITIVE)
                _positiveTestsByDate.Insert(testByDate);

            // District
            var district = GetDistrict(pcrTest.DistrictId);
            district.AddTestByDate(testByDate, positivity);

            // Region
            var region = GetRegion(pcrTest.RegionId);
            region.AddTestByDate(testByDate, positivity);

            // Workplace
            var workplace = GetWorkplace(pcrTest.WorkplaceId);
            workplace.AddTestByDate(testByDate);

            return ERR_NO_ERROR;
        }

        public int RemoveTest(Guid testGuid)
        {
            // temp, pretože musím nájsť originálny test - aby som mal čas testu pre komparátor podľa času
            var tempTestById = new PCRTestByIdAddr(testGuid);
            var testById = _allTestsByIds.Find(tempTestById, out var foundTest);

            if (!foundTest)
                return ERR_OBJECT_NOT_FOUND;

            var test = GetTestAt(testById.PCRTestAddr);
            var patient = GetPatient(test.PatientId, out _);

            // Patient
            var testByDate = new PCRTestByDateAddr(test);
            patient.RemoveTestByDate(testByDate);

            return RemoveTestPartialNonSafe(test);
        }

        // Ak kraj neexisutje, vytvorí sa
        private Region GetRegion(int regionId)
        {
            var region = _regions.Find(new Region(regionId), out var found);

            if (!found)
            {
                region = new Region(regionId);
                _regions.Insert(region);
            }

            return region;
        }


        // Ak pracovisko neexisutje, vytvorí sa
        private Workplace GetWorkplace(int workplaceId)
        {
            var workplace = _workplaces.Find(new Workplace(workplaceId), out var found);

            if (!found)
            {
                workplace = new Workplace(workplaceId);
                _workplaces.Insert(workplace);
            }

            return workplace;
        }

        private District GetDistrict(int districtId)
        {
            var district = _districts.Find(new District(districtId), out var found);

            if (!found)
            {
                district = new District(districtId);
                _districts.Insert(district);
            }

            return district;
        }

        public int AddTest(DateTime timeStamp, string patientId, int workplaceId, int districtId, int regionId,
            bool result, string note)
        {
            // pcrTest tu má iba dočasného pacienta - bez mena a priezviska 
            var pcrTest = new PCRTest(timeStamp, patientId, workplaceId, districtId, regionId, result,
                note);
            return AddTest(pcrTest);
        }

        public int AddTest(Guid guid, DateTime timeStamp, string patientId, int workplaceId, int districtId,
            int regionId, bool result, string note)
        {
            // pcrTest tu má iba dočasného pacienta - bez mena a priezviska 
            var pcrTest = new PCRTest(guid, timeStamp, patientId, workplaceId, districtId, regionId,
                result, note);
            return AddTest(pcrTest);
        }

        public List<ShellPCRTest> GetTestsInDistrictInInterval(int districtId, DateTime minDate, DateTime maxDate,
            int mode, out int err)
        {
            var district = _districts.Find(new District(districtId), out var found);

            if (!found)
            {
                err = ERR_OBJECT_NOT_FOUND;
                return null;
            }

            err = ERR_NO_ERROR;

            LinkedList<long> testAddresses;

            if (mode == Evidence.GET_ALL_TESTS)
                testAddresses = district.GetAllTestsInIntervalAddr(minDate, maxDate);
            else // if (mode == Evidence.GET_POSITIVE_TESTS)
                testAddresses = district.GetPositiveTestsInIntervalAddr(minDate, maxDate);

            var shellTests = AddressesToShellTests(testAddresses);
            return shellTests;
        }

        public List<ShellPCRTest> GetTestsInRegionInInterval(int regionId, DateTime minDate, DateTime maxDate,
            int mode, out int err)
        {
            var region = _regions.Find(new Region(regionId), out var found);

            if (!found)
            {
                err = ERR_OBJECT_NOT_FOUND;
                return null;
            }

            err = ERR_NO_ERROR;

            LinkedList<long> testAddresses;

            if (mode == Evidence.GET_ALL_TESTS)
                testAddresses = region.GetAllTestsInIntervalAddr(minDate, maxDate);
            else // if (mode == Evidence.GET_POSITIVE_TESTS)
                testAddresses = region.GetPositiveTestsInIntervalAddr(minDate, maxDate);

            var shellTests = AddressesToShellTests(testAddresses);
            return shellTests;
        }

        public List<ShellPCRTest> GetTestsInWorkplaceInInterval(int workplaceId, DateTime minDate,
            DateTime maxDate, out int err)
        {
            var workplace = _workplaces.Find(new Workplace(workplaceId), out var found);

            if (!found)
            {
                err = ERR_OBJECT_NOT_FOUND;
                return null;
            }

            err = ERR_NO_ERROR;

            var testAddresses = workplace.GetAllTestsInIntervalAddr(minDate, maxDate);
            var shellTests = AddressesToShellTests(testAddresses);
            return shellTests;
        }

        public List<ShellPCRTest> GetTestsOfPatient(string patientId, out int err)
        {
            var patient = GetPatient(patientId, out var found);

            if (!found)
            {
                err = ERR_OBJECT_NOT_FOUND;
                return null;
            }

            err = ERR_NO_ERROR;

            var testAddresses = patient.GetAllTestsAddr();

            var tests = AddressesToShellTests(testAddresses);

            return tests;
        }

        public List<ShellPCRTest> GetTestsInInterval(DateTime minDate, DateTime maxDate, int mode, out int err)
        {
            var minPCRTest = new PCRTestByDateAddr(new PCRTest(minDate, _guidMin));
            var maxPCRTest = new PCRTestByDateAddr(new PCRTest(maxDate, _guidMax));

            err = ERR_NO_ERROR;

            BTree3<PCRTestByDateAddr> tree;

            if (mode == GET_ALL_TESTS)
                tree = _allTestsByDate;
            else // if (mode == Evidence.GET_POSITIVE_TESTS)
                tree = _positiveTestsByDate;

            var result = new List<ShellPCRTest>(tree.Count);

            foreach (var pcrTest in tree.GetIntervalEnumerator(minPCRTest, maxPCRTest))
            {
                var testAddr = pcrTest.PCRTestAddr;
                var test = GetTestAt(testAddr);

                var patient = GetPatient(test.PatientId, out _);

                result.Add(new ShellPCRTest(test, patient));
            }

            return result;
        }


        public List<ShellPCRTest> GetAllShellTests()
        {
            var result = new List<ShellPCRTest>(_allTestsByIds.Count);

            foreach (var pcrTest in _allTestsByIds)
            {
                var testAddr = pcrTest.PCRTestAddr;
                var test = GetTestAt(testAddr);

                var patient = GetPatient(test.PatientId, out _);

                result.Add(new ShellPCRTest(test, patient));
            }

            return result;
        }

        public List<ShellDistrict> GetDistrictsByPositivityInInterval(DateTime minDate, DateTime maxDate)
        {
            var result = new List<ShellDistrict>();

            foreach (var districtById in _districts)
            {
                var district = GetDistrict(districtById.Id);
                var positiveCount = district.GetPositiveTestsInIntervalCount(minDate, maxDate);
                var shellDistrict = new ShellDistrict(districtById.Id, positiveCount);
                result.Add(shellDistrict);
            }

            result.Sort((x, y) => -x.PositiveCount.CompareTo(y.PositiveCount));

            return result;
        }

        public List<ShellRegion> GetRegionsByPositivityInInterval(DateTime minDate, DateTime maxDate)
        {
            var result = new List<ShellRegion>();

            foreach (var regionById in _regions)
            {
                var region = GetRegion(regionById.Id);
                var positiveCount = region.GetPositiveTestsInIntervalCount(minDate, maxDate);
                var shellRegion = new ShellRegion(regionById.Id, positiveCount);
                result.Add(shellRegion);
            }

            result.Sort((x, y) => -x.PositiveCount.CompareTo(y.PositiveCount));

            return result;
        }

        public List<ShellPCRTest> AddressesToShellTests(IEnumerable<long> addresses)
        {
            var res = new List<ShellPCRTest>(addresses.Count());

            foreach (var address in addresses)
            {
                var test = GetTestAt(address);
                var patient = GetPatient(test.PatientId, out _);

                var shellPCRTest = new ShellPCRTest(test, patient);
                res.Add(shellPCRTest);
            }

            return res;
        }

        public void Dispose()
        {
            _heapTests?.Dispose();
            _patients?.Dispose();
            _workplaces?.Dispose();
            _districts?.Dispose();
            _regions?.Dispose();
            _allTestsByIds?.Dispose();
            _allTestsByDate?.Dispose();
            _positiveTestsByDate?.Dispose();
        }

        public IEnumerable<string> GetPatientsFileContent()
        {
            return _patients.GetFileContent();
        }

        public IEnumerable<string> GetTestsFileContent()
        {
            return _heapTests.GetFileContent();
        }

        public IEnumerable<string> GetDistrictsFileContent()
        {
            return _districts.GetFileContent();
        }

        public IEnumerable<string> GetRegionsFileContent()
        {
            return _regions.GetFileContent();
        }

        public IEnumerable<string> GetWorkplacesFileContent()
        {
            return _workplaces.GetFileContent();
        }

        public IEnumerable<string> GetDistrictFileContent(int id, bool isPositiveFile, out bool found)
        {
            var district = _districts.Find(new District(id), out found);

            if (!found)
                return default;

            if (isPositiveFile)
            {
                return district.GetPositiveTestsFileContent();
            }
            else
            {
                return district.GetAllTestsFileContent();
            }
        }

        public IEnumerable<string> GetRegionFileContent(int id, bool isPositiveFile, out bool found)
        {
            var region = _regions.Find(new Region(id), out found);

            if (!found)
                return default;

            if (isPositiveFile)
            {
                return region.GetPositiveTestsFileContent();
            }
            else
            {
                return region.GetAllTestsFileContent();
            }
        }

        public IEnumerable<string> GetWorkplaceFileContent(int id, out bool found)
        {
            var workplace = _workplaces.Find(new Workplace(id), out found);
            if (!found)
                return default;

            return workplace.GetAllTestsFileContent();
        }

        public IEnumerable<string> GetPatientFileContent(string id, out bool found)
        {
            var patient = GetPatient(id, out found);

            if (!found)
                return default;

            return patient.GetAllTestsFileContent();
        }
    }
}