﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bogus;
using Caliburn.Micro;

namespace PCRTestsEvidence.Models
{
    public class Generator
    {
        private const int MAX_NOTE_SIZE = 20;
        private readonly Evidence _evidence;

        public Generator(Evidence evidence)
        {
            _evidence = evidence;
        }

        public int GeneratePatients(int count)
        {
            var rand = new Random();

            var faker = new Faker("sk");

            var generatedPatients = 0;

            while (generatedPatients < count)
            {
                var name = faker.Name.FirstName();
                var surname = faker.Name.LastName();

                var randomDays = rand.Next(2 * 365, 85 * 365);
                var birthdate = DateTime.Now.AddDays(-randomDays);

                if (_evidence.AddPatient(name, surname, birthdate))
                    generatedPatients++;
            }

            return generatedPatients;
        }

        // Testy vygeneruje pre existujúcich pacientov - za posledných 20 dní
        public int GenerateTests(int count)
        {
            var patients = _evidence.GetAllShellPatients();

            if (patients == null || patients.Count == 0)
                return 0;

            var rand = new Random();

            var faker = new Faker("sk");

            var patientsCount = patients.Count;

            for (var i = 0; i < count; i++)
            {
                var patient = patients[rand.Next(patientsCount)];

                var randomMinutes = rand.Next(0, 24 * 60 * 20);
                var timeStamp = DateTime.Now.AddMinutes(-randomMinutes);
                timeStamp = timeStamp.AddTicks(-(timeStamp.Ticks % (60 * 10000000))); // remove seconds
                var district = rand.Next(1, 80);
                var region = rand.Next(1, 9);
                var workplace = rand.Next(1, 3001);
                var note = faker.Lorem.Slug(3);
                if (note.Length > MAX_NOTE_SIZE)
                    note = note.Substring(0, MAX_NOTE_SIZE);
                var result = rand.NextDouble() < 0.2;

                _evidence.AddTest(timeStamp, patient.Id, workplace, district, region, result, note);
            }

            return count;
        }
    }
}