﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace PCRTestsEvidence.Models
{
    public class PCRTestByIdAddr : IComparable<PCRTestByIdAddr>, IRecord
    {
        public Guid Id { get; protected set; }
        public long PCRTestAddr { get; private set; }

        public PCRTestByIdAddr()
        {
        }

        public override string ToString()
        {
            return $"TEST {Id} @{PCRTestAddr}";
        }

        public PCRTestByIdAddr(Guid id, long pcrTestAddr = -1)
        {
            Id = id;
            PCRTestAddr = pcrTestAddr;
        }

        public byte[] ToByteArray()
        {
            var testIdBytes = Id.ToByteArray(); // byte[16]
            var pcrTestAddrBytes = BitConverter.GetBytes(PCRTestAddr); // long

            return testIdBytes
                .Concat(pcrTestAddrBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var testIdBytes = array[..16]; // byte[16]
            array = array[16..];

            var pcrTestAddrBytes = array; // long

            Id = new Guid(testIdBytes);
            PCRTestAddr = BitConverter.ToInt64(pcrTestAddrBytes);
        }

        public int ByteSize()
        {
            return sizeof(long) + 16;
        }

        public int CompareTo(PCRTestByIdAddr other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }
    }
}