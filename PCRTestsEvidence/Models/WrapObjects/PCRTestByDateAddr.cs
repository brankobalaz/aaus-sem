﻿using System;
using System.Linq;
using HeapFile;

namespace PCRTestsEvidence.Models
{
    public class PCRTestByDateAddr : IRecord, IComparable<PCRTestByDateAddr>
    {
        public Guid TestId { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public long PCRTestAddr { get; private set; }

        public PCRTestByDateAddr()
        {
        }

        public PCRTestByDateAddr(PCRTest pcrTest, long pcrTestAddr = -1)
        {
            TestId = pcrTest.TestId;
            TimeStamp = pcrTest.TimeStamp;
            PCRTestAddr = pcrTestAddr;
        }

        public override string ToString()
        {
            return $"TEST {TestId} @{PCRTestAddr}";
        }
        public int CompareTo(PCRTestByDateAddr other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var timeStampComparison = TimeStamp.CompareTo(other.TimeStamp);
            if (timeStampComparison != 0) return timeStampComparison;
            return TestId.CompareTo(other.TestId);
        }

        public byte[] ToByteArray()
        {
            var timestampBytes = BitConverter.GetBytes(TimeStamp.Ticks); // long
            var testIdBytes = TestId.ToByteArray(); // byte[16]
            var pcrTestAddrBytes = BitConverter.GetBytes(PCRTestAddr); // long

            return timestampBytes
                .Concat(testIdBytes)
                .Concat(pcrTestAddrBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var timestampBytes = array[..sizeof(long)]; // long
            array = array[sizeof(long)..];

            var testIdBytes = array[..16]; // byte[16]
            array = array[16..];

            var pcrTestAddrBytes = array; // long

            TimeStamp = DateTime.FromBinary(BitConverter.ToInt64(timestampBytes));
            TestId = new Guid(testIdBytes);
            PCRTestAddr = BitConverter.ToInt64(pcrTestAddrBytes);
        }

        public int ByteSize()
        {
            return 2 * sizeof(long) + 16;
        }
    }
}