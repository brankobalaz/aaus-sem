﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models.MainObjects;

namespace PCRTestsEvidence.Models
{
    public class Workplace : ObjectWithIntIdAndTestsByDate
    {
        private static string _typeFileName = "./data/workplaces/workplace_";

        public Workplace()
        {
            BTreeTypeFileName = _typeFileName;
        }

        public Workplace(int workplaceId) : base(workplaceId, _typeFileName)
        {
        }

        public override string ToString()
        {
            return $"W {Id}";
        }
    }
}