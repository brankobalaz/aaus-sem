﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace PCRTestsEvidence.Models.MainObjects
{
    public class ObjectWithIntIdAndTestsByDate : ObjectWithIdAndTestsByDate<int>, IComparable<ObjectWithIntIdAndTestsByDate>, IRecord
    {
        protected ObjectWithIntIdAndTestsByDate()
        {
        }

        protected ObjectWithIntIdAndTestsByDate(int id, string bTreeFileName) : base(id, bTreeFileName)
        {
        }

        public int CompareTo(ObjectWithIntIdAndTestsByDate other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Id.CompareTo(other.Id);
        }

        public byte[] ToByteArray()
        {
            return BitConverter.GetBytes(Id);
        }

        public void LoadFromByteArray(byte[] array)
        {
            Id = BitConverter.ToInt32(array);
        }

        public int ByteSize()
        {
            return sizeof(int);
        }
    }
}
