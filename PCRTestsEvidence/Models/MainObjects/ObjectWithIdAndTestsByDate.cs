﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTree3;

namespace PCRTestsEvidence.Models
{
    public abstract class ObjectWithIdAndTestsByDate<T>
    {
        public T Id { get; protected set; }
        protected string BTreeTypeFileName { get; set; }
        protected BTree3<PCRTestByDateAddr> AllTestsByDate { get; set; }

        protected readonly Guid _guidMin = new Guid(new byte[16]);

        protected readonly Guid _guidMax = new Guid(new byte[]
            {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});

        protected ObjectWithIdAndTestsByDate()
        {
        }

        protected ObjectWithIdAndTestsByDate(T id, string bTreeFileName)
        {
            Id = id;
            BTreeTypeFileName = bTreeFileName;
        }

        protected LinkedList<long> GetTestsInIntervalAddr(BTree3<PCRTestByDateAddr> tree, DateTime minDate,
            DateTime maxDate)
        {
            var minPCRTest = new PCRTestByDateAddr(new PCRTest(minDate, _guidMin));
            var maxPCRTest = new PCRTestByDateAddr(new PCRTest(maxDate, _guidMax));

            var result = new LinkedList<long>();

            foreach (var pcrTest in tree.GetIntervalEnumerator(minPCRTest, maxPCRTest))
            {
                result.AddLast(pcrTest.PCRTestAddr);
            }

            return result;
        }

        protected int GetTestsInIntervalCount(BTree3<PCRTestByDateAddr> tree, DateTime minDate, DateTime maxDate)
        {
            var minPCRTest = new PCRTestByDateAddr(new PCRTest(minDate, _guidMin));
            var maxPCRTest = new PCRTestByDateAddr(new PCRTest(maxDate, _guidMax));

            return tree.GetIntervalEnumerator(minPCRTest, maxPCRTest).Count();
        }

        public LinkedList<long> GetAllTestsInIntervalAddr(DateTime minDate, DateTime maxDate)
        {
            LoadAllTestsTree();
            var res =  GetTestsInIntervalAddr(AllTestsByDate, minDate, maxDate);
            CloseAllTestsTree();
            return res;
        }

        public List<long> GetAllTestsAddr()
        {
            LoadAllTestsTree();
            var res = AllTestsByDate.Select(x => x.PCRTestAddr).ToList();
            CloseAllTestsTree();
            return res;
        }

        public virtual bool AddTestByDate(PCRTestByDateAddr pcrTestByDateAddr, bool isPositive = false)
        {
            LoadAllTestsTree();
            var res = AllTestsByDate.Insert(pcrTestByDateAddr);
            CloseAllTestsTree();
            return res;
        }

        public virtual bool RemoveTestByDate(PCRTestByDateAddr pcrTestByDateAddr, bool isPositive = false)
        {
            LoadAllTestsTree();
            var res = AllTestsByDate.Remove(pcrTestByDateAddr);
            CloseAllTestsTree();
            return res;
        }

        public IEnumerable<string> GetAllTestsFileContent()
        {
            LoadAllTestsTree();
            var res = AllTestsByDate.GetFileContent();
            CloseAllTestsTree();
            return res;
        }

        public virtual void Clear()
        {
            LoadAllTestsTree();
            AllTestsByDate.Clear();
            CloseAllTestsTree();
        }

        private void LoadAllTestsTree()
        {
            AllTestsByDate = new BTree3<PCRTestByDateAddr>(BTreeTypeFileName + Id + ".data");
        }

        private void CloseAllTestsTree()
        {
            AllTestsByDate.Dispose();
            AllTestsByDate = null;
        }
    }
}