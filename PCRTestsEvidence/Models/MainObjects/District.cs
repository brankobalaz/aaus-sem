﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models.MainObjects;

namespace PCRTestsEvidence.Models
{
    public class District : ObjectWithIntIdAndTestsByDateByPositivity
    {
        private static string _typeFileName = "./data/districts/district_";

        public District()
        {
            BTreeTypeFileName = _typeFileName;
        }

        public District(int districtId) : base(districtId, _typeFileName)
        {
        }
        public override string ToString()
        {
            return $"D {Id}";
        }
    }
}