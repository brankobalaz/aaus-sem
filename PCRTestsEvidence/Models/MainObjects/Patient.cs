﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTree3;
using HeapFile;

namespace PCRTestsEvidence.Models
{
    public class Patient : ObjectWithIdAndTestsByDate<string>, IComparable<Patient>, IRecord
    {
        private const int MAX_NAME_SIZE = 15;
        private const int MAX_SURNAME_SIZE = 20;
        private const int MAX_ID_SIZE = 10;
        private static string _typeFileName = "./data/patients/patient_";
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public DateTime Birthday { get; private set; }

        public Patient()
        {
            BTreeTypeFileName = _typeFileName;
        }

        public Patient(string id) : base(id, _typeFileName)
        {
        }

        public Patient(string name, string surname, DateTime birthday, string patientId) : base(patientId,
            _typeFileName)
        {
            Name = name;
            Surname = surname;
            Birthday = birthday;
        }

        public override string ToString()
        {
            return $"{Name} {Surname}, {Birthday.ToShortDateString()}, {Id}";
        }

        public int CompareTo(Patient other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(Id, other.Id, StringComparison.Ordinal);
        }
        public byte[] ToByteArray()
        {
            var idLengthBytes = BitConverter.GetBytes(Id.Length);
            var tempId = (Id ?? "").PadRight(MAX_ID_SIZE, '*');
            var idBytes = Encoding.Unicode.GetBytes(tempId.ToCharArray());

            var birthdayBytes = BitConverter.GetBytes(Birthday.Ticks); // long

            var nameValidBytes = BitConverter.GetBytes((Name ?? "").Length); // int

            var tempName = (Name ?? "").PadRight(MAX_NAME_SIZE, '*');
            var nameBytes = Name == null
                ? new byte[MAX_NAME_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempName.ToCharArray());

            var surnameValidBytes = BitConverter.GetBytes((Surname ?? "").Length); // int 
            var tempSurname = (Surname ?? "").PadRight(MAX_SURNAME_SIZE, '*');
            var surnameBytes = Surname == null
                ? new byte[MAX_SURNAME_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempSurname.ToCharArray());

            return idLengthBytes
                .Concat(idBytes)
                .Concat(birthdayBytes)
                .Concat(nameValidBytes)
                .Concat(nameBytes)
                .Concat(surnameValidBytes)
                .Concat(surnameBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var idLengthBytes = array[..sizeof(int)];
            array = array[sizeof(int)..];

            var idBytes = array[.. (MAX_ID_SIZE * sizeof(char))];
            array = array[(MAX_ID_SIZE * sizeof(char))..];

            var birthdayBytes = array[..sizeof(long)]; // long
            array = array[sizeof(long)..];

            var nameLengthBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var nameBytes = array[.. (MAX_NAME_SIZE * sizeof(char))];
            array = array[(MAX_NAME_SIZE * sizeof(char))..];

            var surnameLengthBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var surnameBytes = array;

            var idLength = BitConverter.ToInt32(idLengthBytes);
            Id = Encoding.Unicode.GetString(idBytes[.. (idLength * sizeof(char))]);

            Birthday = DateTime.FromBinary(BitConverter.ToInt64(birthdayBytes));

            var nameLength = BitConverter.ToInt32(nameLengthBytes);
            Name = Encoding.Unicode.GetString(nameBytes[..(nameLength * sizeof(char))]);

            var surnameLength = BitConverter.ToInt32(surnameLengthBytes);
            Surname = Encoding.Unicode.GetString(surnameBytes[..(surnameLength * sizeof(char))]);
        }

        public int ByteSize()
        {
            return sizeof(long) + 3 * sizeof(int) + (MAX_SURNAME_SIZE + MAX_NAME_SIZE + MAX_ID_SIZE) * sizeof(char);
        }
    }
}