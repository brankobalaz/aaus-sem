﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTree3;

namespace PCRTestsEvidence.Models
{
    public abstract class ObjectWithIdAndTestsByDateByPositivity<T> : ObjectWithIdAndTestsByDate<T>
    {
        protected BTree3<PCRTestByDateAddr> PositiveTestsByDate { get; set; }

        protected ObjectWithIdAndTestsByDateByPositivity()
        {
        }

        protected ObjectWithIdAndTestsByDateByPositivity(T id, string bTreeFileName) : base(id, bTreeFileName)
        {
        }

        public LinkedList<long> GetPositiveTestsInIntervalAddr(DateTime minDate, DateTime maxDate)
        {
            LoadPositiveTestsTree();
            var res =  GetTestsInIntervalAddr(PositiveTestsByDate, minDate, maxDate);
            ClosePositiveTestsTree();
            return res;
        }

        public int GetPositiveTestsInIntervalCount(DateTime minDate, DateTime maxDate)
        {
            LoadPositiveTestsTree();
            var res=  GetTestsInIntervalCount(PositiveTestsByDate, minDate, maxDate);
            ClosePositiveTestsTree();
            return res;
        }

        public override bool AddTestByDate(PCRTestByDateAddr pcrTestByDateAddr, bool isPositive)
        {
            if (!base.AddTestByDate(pcrTestByDateAddr))
                return false;

            if (isPositive)
            {
                LoadPositiveTestsTree();
                PositiveTestsByDate.Insert(pcrTestByDateAddr);
                ClosePositiveTestsTree();
            }
            
            return true;
        }

        public override bool RemoveTestByDate(PCRTestByDateAddr pcrTestByDateAddr, bool isPositive)
        {
            if (!base.RemoveTestByDate(pcrTestByDateAddr))
                return false;

            if (isPositive)
            {
                LoadPositiveTestsTree();
                PositiveTestsByDate.Remove(pcrTestByDateAddr);
                ClosePositiveTestsTree();
            }
            
            return true;
        }

        public IEnumerable<string> GetPositiveTestsFileContent()
        {
            LoadPositiveTestsTree();
            var res = PositiveTestsByDate.GetFileContent();
            ClosePositiveTestsTree();
            return res;
        }
        public override void Clear()
        {
            LoadPositiveTestsTree();
            PositiveTestsByDate.Clear();
            ClosePositiveTestsTree();

            base.Clear();
        }

        private void LoadPositiveTestsTree()
        {
            PositiveTestsByDate = new BTree3<PCRTestByDateAddr>(BTreeTypeFileName + Id + "_positive.data");
        }

        private void ClosePositiveTestsTree()
        {
            PositiveTestsByDate.Dispose();
            PositiveTestsByDate = null;
        }
    }
}