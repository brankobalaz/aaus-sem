﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models.MainObjects;

namespace PCRTestsEvidence.Models
{
    public class Region : ObjectWithIntIdAndTestsByDateByPositivity
    {
        private static string _typeFileName = "./data/regions/region_";

        public Region()
        {
            BTreeTypeFileName = _typeFileName;
        }

        public Region(int regionId) : base(regionId, _typeFileName)
        {
        }

        public override string ToString()
        {
            return $"R {Id}";
        }
    }
}