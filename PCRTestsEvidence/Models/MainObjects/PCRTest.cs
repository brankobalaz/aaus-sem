﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace PCRTestsEvidence.Models
{
    public class PCRTest : IRecord
    {
        private const int MAX_NOTE_SIZE = 20;
        private const int MAX_PATIENT_ID_SIZE = 10;
        private const string NOT_FOUND = "";

        public const bool POSITIVE = true;
        public const bool NEGATIVE = false;
        public DateTime TimeStamp { get; private set; }
        public string PatientId { get; private set; }
        public Guid TestId { get; private set; }
        public int WorkplaceId { get; private set; }
        public int DistrictId { get; private set; }
        public int RegionId { get; private set; }
        public bool Result { get; private set; }
        public string Note { get; private set; }

        public PCRTest()
        {
        }

        public PCRTest(Guid guid)
        {
            TestId = guid;
        }

        public PCRTest(DateTime date, Guid guid)
        {
            TimeStamp = date;
            TestId = guid;
        }

        public PCRTest(DateTime timeStamp, string patientId, int workplaceId, int districtId, int regionId,
            bool result, string note)
        {
            TimeStamp = timeStamp;
            PatientId = patientId;
            WorkplaceId = workplaceId;
            DistrictId = districtId;
            RegionId = regionId;
            Result = result;
            Note = note;
            TestId = Guid.NewGuid();
        }

        public PCRTest(Guid guid, DateTime timeStamp, string patientId, int workplaceId, int districtId,
            int regionId, bool result, string note)
        {
            TimeStamp = timeStamp;
            PatientId = PatientId;
            WorkplaceId = workplaceId;
            DistrictId = districtId;
            RegionId = regionId;
            Result = result;
            Note = note;
            TestId = guid;
        }

        public bool ReplaceTempPatient(string patientId)
        {
            // Iba ak je pacient naozaj temp
            if (PatientId == NOT_FOUND)
            {
                PatientId = patientId;
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return
                $"PAT_ID: {PatientId}, Time: {TimeStamp}, W: {WorkplaceId}, D: {DistrictId}, R: {RegionId}, >: {(Result ? "POS" : "NEG")}, ID: {TestId}, Note: \"{Note}\"";
        }

        public byte[] ToByteArray()
        {
            var timestampBytes = BitConverter.GetBytes(TimeStamp.Ticks); // long

            var patientIdLengthBytes = BitConverter.GetBytes((PatientId ?? "").Length); // int
            var tempPatientId = (PatientId ?? "").PadRight(MAX_PATIENT_ID_SIZE, '*');
            var patientIdBytes = PatientId == null
                ? new byte[MAX_PATIENT_ID_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempPatientId.ToCharArray()); // MAX_PATIENT_ID_LENGTH * sizeof(char)

            var testIdBytes = TestId.ToByteArray(); // byte[16]
            var workplaceIdBytes = BitConverter.GetBytes(WorkplaceId); // int
            var districtIdBytes = BitConverter.GetBytes(DistrictId); // int
            var regionIdBytes = BitConverter.GetBytes(RegionId); // int
            var resultBytes = BitConverter.GetBytes(Result); // bool

            var noteLengthBytes = BitConverter.GetBytes((Note ?? "").Length); // int
            var tempNote = (Note ?? "").PadRight(MAX_NOTE_SIZE, '*');
            var noteBytes = Note == null
                ? new byte[MAX_NOTE_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempNote.ToCharArray()); // MAX_NOTE_LENGTH * sizeof(char)

            return timestampBytes
                .Concat(patientIdLengthBytes)
                .Concat(patientIdBytes)
                .Concat(testIdBytes)
                .Concat(workplaceIdBytes)
                .Concat(districtIdBytes)
                .Concat(regionIdBytes)
                .Concat(resultBytes)
                .Concat(noteLengthBytes)
                .Concat(noteBytes)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var timestampBytes = array[..sizeof(long)]; // long
            array = array[sizeof(long)..];

            var patientIdLengthBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var patientIdBytes = array[..(MAX_PATIENT_ID_SIZE * sizeof(char))]; // MAX_PATIENT_ID_LENGTH * sizeof(char)
            array = array[(MAX_PATIENT_ID_SIZE * sizeof(char))..];

            var testIdBytes = array[..16]; // byte[16]
            array = array[16..];

            var workplaceIdBytes = array[.. sizeof(int)]; // int
            array = array[sizeof(int)..];

            var districtIdBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var regionIdBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var resultBytes = array[..sizeof(bool)]; // bool
            array = array[sizeof(bool)..];

            var noteLengthBytes = array[..sizeof(int)]; // int
            array = array[sizeof(int)..];

            var noteBytes = array; // MAX_NOTE_LENGTH * sizeof(char)

            TimeStamp = DateTime.FromBinary(BitConverter.ToInt64(timestampBytes));
            var patientIdLength = BitConverter.ToInt32(patientIdLengthBytes);
            PatientId = Encoding.Unicode.GetString(patientIdBytes[..(sizeof(char) * patientIdLength)]);
            WorkplaceId = BitConverter.ToInt32(workplaceIdBytes);
            DistrictId = BitConverter.ToInt32(districtIdBytes);
            RegionId = BitConverter.ToInt32(regionIdBytes);
            Result = BitConverter.ToBoolean(resultBytes);
            var noteLength = BitConverter.ToInt32(noteLengthBytes);
            Note = Encoding.Unicode.GetString(noteBytes[..(sizeof(char) * noteLength)]);
            TestId = new Guid(testIdBytes);
        }

        public int ByteSize()
        {
            return sizeof(bool) + sizeof(long) + 5 * sizeof(int) + 16 +
                   (MAX_NOTE_SIZE + MAX_PATIENT_ID_SIZE) * sizeof(char);
        }
    }
}