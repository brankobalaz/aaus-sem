﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRTestsEvidence.Models
{
    public class ShellPCRTest
    {
        public const bool POSITIVE = true;
        public const bool NEGATIVE = false;
        public DateTime TimeStamp { get; }
        public ShellPatient Patient { get; }
        public Guid TestId { get; }
        public int WorkplaceId { get; }
        public int DistrictId { get; }
        public int RegionId { get; }
        public bool Result { get; }
        public string Note { get; }

        public ShellPCRTest(PCRTest test, Patient patient)
        {
            TimeStamp = test.TimeStamp;
            Patient = new ShellPatient(patient);
            WorkplaceId = test.WorkplaceId;
            DistrictId = test.DistrictId;
            RegionId = test.RegionId;
            Result = test.Result;
            Note = test.Note;
            TestId = test.TestId;
        }
        public override string ToString()
        {
            return $"Pacient: [{Patient}], Čas: {TimeStamp}, P: {WorkplaceId}, O: {DistrictId}, K: {RegionId}, V: {(Result ? "POS" : "NEG")}, ID: {TestId}, Pozn.: \"{Note}\"";
        }
    }
}
