﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRTestsEvidence.Models
{
    public class ShellDistrict : ShellObjectWithIdAndPositivityCount
    {
        public ShellDistrict(int id, int positiveCount) : base(id, positiveCount)
        {
        }
    }
}
