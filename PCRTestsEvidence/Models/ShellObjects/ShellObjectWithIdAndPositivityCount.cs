﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRTestsEvidence.Models
{
    public class ShellObjectWithIdAndPositivityCount
    {
        public int Id { get; }
        public int PositiveCount { get; }

        public ShellObjectWithIdAndPositivityCount(int id, int positiveCount)
        {
            Id = id;
            PositiveCount = positiveCount;
        }
    }
}
