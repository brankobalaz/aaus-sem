﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCRTestsEvidence.Models
{
    public class ShellPatient
    {
        public string Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public DateTime Birthday { get; }

        public ShellPatient(Patient patient)
        {
            Id = patient.Id;
            Name = patient.Name;
            Surname = patient.Surname;
            Birthday = patient.Birthday;
        }

        public override string ToString()
        {
            return $"{Name} {Surname}, {Birthday.ToShortDateString()}, {Id}";
        }
    }
}