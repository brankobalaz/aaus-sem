﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public abstract class ShowSickPatientsViewModelTemplate : Screen
    {
        protected Evidence _evidence;
        protected string _objectName;
        public DateTime Date { get; set; }
        public int Id { get; set; }
        public int QuarantineDays { get; set; }
        public int ListDataCount { get; set; }
        public BindableCollection<ShellPCRTest> ListOfTests { get; }

        protected ShowSickPatientsViewModelTemplate(string objectName, Evidence evidence)
        {
            _objectName = objectName;
            _evidence = evidence;
            QuarantineDays = 5;
            Date = DateTime.Now;
            ListDataCount = 0;
            ListOfTests = new BindableCollection<ShellPCRTest>();
        }

        protected abstract List<ShellPCRTest> GetTests(out int err);

        public void ShowSick()
        {
            var tests = GetTests(out var err);

            if (err == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show($"{_objectName} so zadaným číslom neexistuje!", "Chyba",
                    MessageBoxButton.OK, MessageBoxImage.Error);

                ListOfTests.Clear();
                NotifyOfPropertyChange(nameof(ListOfTests));

                ListDataCount = 0;
                NotifyOfPropertyChange(nameof(ListDataCount));

                return;
            }

            ListOfTests.Clear();
            ListOfTests.AddRange(tests);
            NotifyOfPropertyChange(nameof(ListOfTests));

            ListDataCount = ListOfTests.Count;
            NotifyOfPropertyChange(nameof(ListDataCount));
        }

        public void CopyPatientIdToClipboard(ShellPCRTest pcrTest)
        {
            Clipboard.SetText(pcrTest.Patient.Id.ToString());
        }
    }
}