﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowSickPatientsInEvidenceViewModel : ShowSickPatientsViewModelTemplate
    {
        public ShowSickPatientsInEvidenceViewModel(Evidence evidence) : base("Evidencia", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(out int err)
        {
            var minDate = Date.AddDays(-QuarantineDays).Date;
            var maxDate = Date.AddDays(1).Date;

            return _evidence.GetTestsInInterval(minDate, maxDate, 
                Evidence.GET_POSITIVE_TESTS, out err);
        }
    }
}