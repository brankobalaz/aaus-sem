﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public abstract class ShowObjectsByPositivityViewModelTemplate<T> : Screen
        where T : ShellObjectWithIdAndPositivityCount
    {
        protected Evidence _evidence;
        protected string _objectName;
        public DateTime Date { get; set; }
        public int QuarantineDays { get; set; }
        public int ListDataCount { get; set; }
        public BindableCollection<T> ListOfObjects { get; }

        protected ShowObjectsByPositivityViewModelTemplate(string objectName, Evidence evidence)
        {
            _objectName = objectName;
            _evidence = evidence;
            Date = DateTime.Now;
            QuarantineDays = 5;
            ListOfObjects = new BindableCollection<T>();
        }

        public void ShowOrder()
        {
            var objects = GetOrder();

            if (objects.Count == 0)
            {
                MessageBox.Show($"V evidencií nie sú žiadne {_objectName}.", "Rebríček",
                    MessageBoxButton.OK, MessageBoxImage.Information);

                ListOfObjects.Clear();
                ListDataCount = 0;
                NotifyOfPropertyChange(nameof(ListOfObjects));
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
            else
            {
                ListOfObjects.Clear();
                ListOfObjects.AddRange(objects);
                NotifyOfPropertyChange(nameof(ListOfObjects));

                ListDataCount = ListOfObjects.Count;
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
        }

        protected abstract List<T> GetOrder();

        public void CopyObjectIdToClipboard(T shellObject)
        {
            Clipboard.SetText(shellObject.Id.ToString());
        }
    }
}