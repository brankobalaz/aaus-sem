﻿using System;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Microsoft.Win32;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShellViewModel : Screen
    {
        private readonly SimpleContainer _container;
        private readonly Evidence _evidence;
        private readonly DataLoader _dataLoader;
        private INavigationService _navigationService;
        public BindableCollection<ViewModelShell> Features { get; }
        public BindableCollection<ViewModelShell> Tools { get; }

        public ShellViewModel(SimpleContainer container, Evidence evidence, DataLoader dataLoader)
        {
            _container = container;
            _evidence = evidence;
            _dataLoader = dataLoader;

            Features = new BindableCollection<ViewModelShell>
            {
                new("Pridaj pacienta / Zoznam pacientov", "Pridá pacienta do evidencie.", typeof(AddPatientViewModel)),
                new("Pridaj test", "Pridaj test do evidencie.", typeof(AddTestViewModel)),
                new("Zobraz/Odstráň test", "Zobrazí/Odstráni test na základe identifikátora testu.", typeof(ShowOrRemoveTestViewModel)),
                new("Zobraz testy pacienta / Odstráň pacienta", "Zobrazí všetky testy pre pacienta za dané časové obdobie a dovolí odstrániť pacienta.", typeof(ShowTestsRemovePatientViewModel)),
                new("Zobraz testy v okrese", "Zobrazí všetky, alebo iba pozitívne testy v okrese za dané časové obdobie.", typeof(ShowTestsByDistrictViewModel)),
                new("Zobraz testy v kraji", "Zobrazí všetky, alebo iba pozitívne testy v kraji za dané časové obdobie.", typeof(ShowTestsByRegionViewModel)),
                new("Zobraz testy v evidecii", "Zobrazí všetky, alebo iba pozitívne testy z celej evidencie za dané časové obdobie.", typeof(ShowTestsByDateViewModel)),
                new("Zobraz testy na pracovisku", "Zobrazí všetky, alebo iba pozitívne testy na pracovisku za dané časové obdobie.", typeof(ShowTestsByWorkplaceViewModel)),
                new("Zobraz chorých pacientov v okrese", "Zobrazí chorých pacientov v daný deň v danom okrese.", typeof(ShowSickPatientsInDistrictViewModel)),
                new("Zobraz chorých pacientov v kraji", "Zobrazí chorých pacientov v daný deň v danom kraji.", typeof(ShowSickPatientsInRegionViewModel)),
                new("Zobraz chorých pacientov v evidencii", "Zobrazí chorých pacientov v evidencii v daný deň.", typeof(ShowSickPatientsInEvidenceViewModel)),
                new("Zobraz okresy podľa počtu chorých", "Zobrazí okresy zoradené podľa počtu chorých pacientov v daný deň.", typeof(ShowDistrictsByPositivityViewModel)),
                new("Zobraz kraje podľa počtu chorých", "Zobrazí kraje zoradené podľa počtu chorých pacientov v daný deň.", typeof(ShowRegionsByPositivityViewModel)),
            };

            Tools = new BindableCollection<ViewModelShell>()
            {
                new("Generátor", "Vygeneruje testy a pacientov do evidencie.", typeof(GeneratorViewModel)),
                new("Prehliadač súborov", "Vypíše obsah súborov evidencie.", typeof(ShowFileContentViewModel)),
            };
        }


        public void ClearEvidence()
        {
            _evidence.ClearEvidence(out var patientsCount, out var testsCount);
            
            MessageBox.Show($"Bolo odstránených {patientsCount} pacientov a {testsCount} testov.", "Správa evidencie",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public async void SaveTestsData()
        {
            var sfd = new SaveFileDialog();
            var filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            sfd.Filter = filter;

            if (sfd.ShowDialog() == false)
                return;

            var testsCount = await _dataLoader.SaveTests(sfd.FileName);

            MessageBox.Show($"Úspešne sa do súboru uložilo {testsCount} testov.", "Správa evidencie",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public async void SavePatientsData()
        {
            var sfd = new SaveFileDialog();
            var filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            sfd.Filter = filter;

            if (sfd.ShowDialog() == false)
                return;

            var patientsCount = await _dataLoader.SavePatients(sfd.FileName);

            MessageBox.Show($"Úspešne sa do súboru uložilo {patientsCount} pacientov.", "Správa evidencie",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public async void LoadTestsData()
        {
            var ofd = new OpenFileDialog();
            var filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            ofd.Filter = filter;

            if (ofd.ShowDialog() == false)
                return;

            var testsCount = await _dataLoader.LoadTests(ofd.FileName);

            MessageBox.Show($"Úspešne sa do evidencie načítalo {testsCount} testov.", "Správa evidencie",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public async void LoadPatientsData()
        {
            var ofd = new OpenFileDialog();
            var filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            ofd.Filter = filter;

            if (ofd.ShowDialog() == false)
                return;

            var patientsCount = await _dataLoader.LoadPatients(ofd.FileName);

            MessageBox.Show($"Úspešne sa do evidencie načítalo {patientsCount} pacientov.", "Správa evidencie",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void RegisterFrame(Frame frame)
        {
            _navigationService = new FrameAdapter(frame);
            _container.Instance(_navigationService);

            _navigationService.NavigateToViewModel(typeof(GeneratorViewModel));
        }

        public void ShowViewModel(ViewModelShell viewModelShell)
        {
            _navigationService.NavigateToViewModel(viewModelShell.ViewModel);
        }
    }
}