﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowSickPatientsInRegionViewModel : ShowSickPatientsViewModelTemplate
    {
        public ShowSickPatientsInRegionViewModel(Evidence evidence) : base("Kraj", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(out int err)
        {
            var minDate = Date.AddDays(-QuarantineDays).Date;
            var maxDate = Date.AddDays(1).Date;

            return _evidence.GetTestsInRegionInInterval(Id, minDate, maxDate,
                Evidence.GET_POSITIVE_TESTS, out err);
        }
    }
}
