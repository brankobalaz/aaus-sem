﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class AddPatientViewModel : Screen
    {
        public BindableCollection<ShellPatient> ListOfPatients { get; private set; }
        private string _name;
        private string _surname;
        public DateTime BirthDate { get; set; }
        public string PatientId { get; set; }
        public int ListDataCount { get; private set; }

        private readonly Evidence _evidence;
        
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyOfPropertyChange(nameof(CanAddPatient));
            }
        }

        public string Surname
        {
            get => _surname;
            set
            {
                _surname = value;
                NotifyOfPropertyChange(nameof(CanAddPatient));
            }
        }

        public AddPatientViewModel(Evidence evidence)
        {
            BirthDate = DateTime.Today;
            _evidence = evidence;

            ListOfPatients = new BindableCollection<ShellPatient>(_evidence.GetAllShellPatients());
            ListDataCount = ListOfPatients.Count;
        }

        public void AddPatient()
        {
            if (!_evidence.AddPatient(_name, _surname, BirthDate, PatientId))
            {
                MessageBox.Show("Pacienta sa nepodarilo pridať!\nPravdepodobne pacient s rovnakým rodným číslom už existuje.", "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ListOfPatients.Clear();
            ListOfPatients.AddRange(_evidence.GetAllShellPatients());
            NotifyOfPropertyChange(nameof(ListOfPatients));

            ListDataCount = ListOfPatients.Count;
            NotifyOfPropertyChange(nameof(ListDataCount));

            MessageBox.Show($"Pacient {_name} {_surname} bol úspešne pridaný.", "Pridanie pacienta", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public bool CanAddPatient => !string.IsNullOrWhiteSpace(_name)
                                     && !string.IsNullOrWhiteSpace(_surname);

        public void CopyPatientIdToClipboard(ShellPatient patient)
        {
            Clipboard.SetText(patient.Id.ToString());
        }
    }
}