﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class AddTestViewModel : Screen
    {
        private readonly Evidence _evidence;
        private bool _resultPositive;
        private bool _resultNegative;
        private string _patientId;
        public string Note { get; set; }
        public int WorkplaceId { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }
        public DateTime TestDate { get; set; }
        public string TestTime { get; set; }

        public bool ResultPositive
        {
            get => _resultPositive;
            set
            {
                _resultPositive = value;
                NotifyOfPropertyChange(nameof(CanAddTest));
            }
        }

        public bool ResultNegative
        {
            get => _resultNegative;
            set
            {
                _resultNegative = value;
                NotifyOfPropertyChange(nameof(CanAddTest));
            }
        }
        public string PatientId
        {
            get => _patientId;
            set
            {
                _patientId = value;
                NotifyOfPropertyChange(nameof(CanAddTest));
            }
        }

        public AddTestViewModel(Evidence evidence)
        {
            _evidence = evidence;
            TestDate = DateTime.Today;
            TestTime = DateTime.Now.ToShortTimeString();
        }

        public void AddTest()
        {
            if (!ParseTimeToDate())
            {
                MessageBox.Show("Čas testu má zlý formát!", "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var res = _evidence.AddTest(TestDate, PatientId, WorkplaceId, DistrictId, RegionId, _resultPositive, Note);

            if (res == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show("Pacient so zadaným rodným číslom neexistuje!\nNajskôr vytvor pacienta.", "Chyba",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("Test bol úspešne pridaný.", "Pridanie testu", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }

        private bool ParseTimeToDate()
        {
            TestDate = TestDate.Date;

            try
            {
                TestDate = TestDate.AddHours(int.Parse((TestTime.Split(':'))[0]));
                TestDate = TestDate.AddMinutes(int.Parse((TestTime.Split(':'))[1]));
            }
            catch
            {
                return false;
            }

            TestTime = TestDate.ToShortTimeString();

            NotifyOfPropertyChange(nameof(TestDate));
            NotifyOfPropertyChange(nameof(TestTime));

            return true;
        }

        public bool CanAddTest => !string.IsNullOrWhiteSpace(PatientId)
                                  && (ResultNegative || ResultPositive);
    }
}