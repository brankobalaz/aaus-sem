﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class GeneratorViewModel : Screen
    {
        private readonly Generator _generator;
        private int _patientCount;
        private int _testCount;

        public int PatientCount
        {
            get => _patientCount;
            set
            {
                _patientCount = value;
                NotifyOfPropertyChange(nameof(CanGeneratePatients));
            }
        }

        public int TestCount
        {
            get => _testCount;
            set
            {
                _testCount = value;
                NotifyOfPropertyChange(nameof(CanGenerateTests));
            }
        }

        public GeneratorViewModel(Generator generator)
        {
            _generator = generator;
        }

        public void GenerateTests()
        {
            var generated = _generator.GenerateTests(TestCount);

            MessageBox.Show($"Bolo vygenerovaných {generated} testov.", "Generátor", MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        public void GeneratePatients()
        {
            var generated = _generator.GeneratePatients(PatientCount);

            MessageBox.Show($"Bolo vygenerovaných {generated} pacientov.", "Generátor", MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        public bool CanGenerateTests => TestCount != 0;
        public bool CanGeneratePatients => PatientCount != 0;
    }
}