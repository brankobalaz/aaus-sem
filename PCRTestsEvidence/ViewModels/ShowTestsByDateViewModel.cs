﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowTestsByDateViewModel : ShowTestsByDateByPositivityViewModelTemplate<int>
    {
        public ShowTestsByDateViewModel(Evidence evidence) : base("", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(int mode, out int err)
        {
            return _evidence.GetTestsInInterval(FromDate, ToDate, mode, out err);
        }
    }
}