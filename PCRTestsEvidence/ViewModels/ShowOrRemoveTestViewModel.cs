﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowOrRemoveTestViewModel : Screen
    {
        private string _testIdString;
        private readonly Evidence _evidence;
        public string TestResult { get; set; }
        public ShellPCRTest PcrTest { get; set; }

        public string TestIdString
        {
            get => _testIdString;
            set
            {
                _testIdString = value;
                NotifyOfPropertyChange(nameof(CanGetTest));
                NotifyOfPropertyChange(nameof(CanRemoveTest));
            }
        }

        public ShowOrRemoveTestViewModel(Evidence evidence)
        {
            _evidence = evidence;
            PcrTest = null;
            TestResult = "";
        }

        public void GetTest()
        {
            PcrTest = _evidence.GetShellTest(_testIdString, out var found);

            if (!found)
            {
                MessageBox.Show("Test so zadaným identifikátorom nebol nájdený!", "Chyba", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            TestResult = PcrTest.Result == ShellPCRTest.POSITIVE ? "Pozitívny" : "Negatívny";
            NotifyOfPropertyChange(nameof(PcrTest));
            NotifyOfPropertyChange(nameof(TestResult));
        }

        public void RemoveTest()
        {
            Guid testGuid;

            try
            {
                testGuid = Guid.Parse(TestIdString);
            }
            catch
            {
                MessageBox.Show("Identifikátor testu má zlý formát!", "Chyba", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            var res = _evidence.RemoveTest(testGuid);

            if (res == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show("Test so zadaným identifikátorom nebol nájdený!", "Chyba", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show($"Test s identifikátorom {TestIdString} bol úspešne odstránený z evidencie!",
                    "Odstránenie testu", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }

            TestResult = "";
            PcrTest = null;
            NotifyOfPropertyChange(nameof(PcrTest));
            NotifyOfPropertyChange(nameof(TestResult));
        }

        public bool CanGetTest => !string.IsNullOrWhiteSpace(_testIdString);
        public bool CanRemoveTest => !string.IsNullOrWhiteSpace(_testIdString);
    }
}