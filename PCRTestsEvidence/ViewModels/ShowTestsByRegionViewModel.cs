﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowTestsByRegionViewModel : ShowTestsByDateByPositivityViewModelTemplate<int>
    {
        public ShowTestsByRegionViewModel(Evidence evidence) : base("Kraj", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(int mode, out int err)
        {
            return _evidence.GetTestsInRegionInInterval(Id, FromDate, ToDate, mode, out err);
        }
    }
}