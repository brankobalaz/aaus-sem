﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowTestsRemovePatientViewModel : Screen
    {
        private readonly Evidence _evidence;
        public int ListDataCount { get; set; }
        public BindableCollection<ShellPCRTest> ListOfTests { get; }
        public string Id { get; set; }

        public ShowTestsRemovePatientViewModel(Evidence evidence)
        {
            _evidence = evidence;
            ListOfTests = new BindableCollection<ShellPCRTest>();
        }

        public void ShowAll()
        {
            var tests = _evidence.GetTestsOfPatient(Id, out var err);

            if (err == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show($"Pacient so zadaným číslom neexistuje!", "Chyba",
                    MessageBoxButton.OK, MessageBoxImage.Error);

                ListOfTests.Clear();
                NotifyOfPropertyChange(nameof(ListOfTests));

                ListDataCount = 0;
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
            else
            {
                ListOfTests.Clear();
                ListOfTests.AddRange(tests);
                NotifyOfPropertyChange(nameof(ListOfTests));

                ListDataCount = ListOfTests.Count;
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
        }

        public void RemovePatient()
        {
            var res = _evidence.RemovePatient(Id);


            if (res == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show("Pacient so zadaným identifikátorom nebol nájdený!", "Chyba", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show(
                    $"Pacient s identifikátorom {Id} bol úspešne odstránený z evidencie spolu s jeho {res} testami.",
                    "Odstránenie pacienta", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }

            ListOfTests.Clear();
            ListDataCount = 0;
            NotifyOfPropertyChange(nameof(ListOfTests));
            NotifyOfPropertyChange(nameof(ListDataCount));
        }
        public void CopyTestIdToClipboard(ShellPCRTest pcrTest)
        {
            Clipboard.SetText(pcrTest.TestId.ToString());
        }
    }
}