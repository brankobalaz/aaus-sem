﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public abstract class ShowTestsByDateByPositivityViewModelTemplate<T> : Screen
    {
        protected Evidence _evidence;
        protected string _objectName;
        public DateTime FromDate { get; set; }
        public string FromTime { get; set; }
        public DateTime ToDate { get; set; }
        public string ToTime { get; set; }
        public int ListDataCount { get; set; }
        public BindableCollection<ShellPCRTest> ListOfTests { get; }
        public T Id { get; set; }

        protected ShowTestsByDateByPositivityViewModelTemplate(string objectName, Evidence evidence)
        {
            _evidence = evidence;
            _objectName = objectName;
            FromDate = DateTime.Now.AddDays(-10);
            FromTime = "00:00";
            ToDate = DateTime.Now;
            ToTime = "23:59";
            ListOfTests = new BindableCollection<ShellPCRTest>();
        }

        protected abstract ICollection<ShellPCRTest> GetTests(int mode, out int err);

        protected void Show(int mode = Evidence.GET_ALL_TESTS)
        {
            if (!ParseTimeToDate())
            {
                MessageBox.Show("Čas od, alebo do má zlý formát!", "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var tests = GetTests(mode, out var err);

            if (err == Evidence.ERR_OBJECT_NOT_FOUND)
            {
                MessageBox.Show($"{_objectName} so zadaným číslom neexistuje!", "Chyba",
                    MessageBoxButton.OK, MessageBoxImage.Error);

                ListOfTests.Clear();
                NotifyOfPropertyChange(nameof(ListOfTests));

                ListDataCount = 0;
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
            else
            {
                ListOfTests.Clear();
                ListOfTests.AddRange(tests);
                NotifyOfPropertyChange(nameof(ListOfTests));

                ListDataCount = ListOfTests.Count;
                NotifyOfPropertyChange(nameof(ListDataCount));
            }
        }

        public void ShowAll()
        {
            Show(Evidence.GET_ALL_TESTS);
        }

        public void ShowPositive()
        {
            Show(Evidence.GET_POSITIVE_TESTS);
        }

        private bool ParseTimeToDate()
        {
            FromDate = FromDate.Date;
            ToDate = ToDate.Date;

            try
            {
                FromDate = FromDate.AddHours(int.Parse((FromTime.Split(':'))[0]));
                FromDate = FromDate.AddMinutes(int.Parse((FromTime.Split(':'))[1]));

                ToDate = ToDate.AddHours(int.Parse((ToTime.Split(':'))[0]));
                ToDate = ToDate.AddMinutes(int.Parse((ToTime.Split(':'))[1]));
            }
            catch
            {
                return false;
            }

            FromTime = FromDate.ToShortTimeString();
            ToTime = ToDate.ToShortTimeString();

            NotifyOfPropertyChange(nameof(FromDate));
            NotifyOfPropertyChange(nameof(FromTime));

            NotifyOfPropertyChange(nameof(ToDate));
            NotifyOfPropertyChange(nameof(ToTime));

            return true;
        }

        public void CopyTestIdToClipboard(ShellPCRTest pcrTest)
        {
            Clipboard.SetText(pcrTest.TestId.ToString());
        }
    }
}