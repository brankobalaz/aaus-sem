﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowFileContentViewModel : Screen
    {
        private readonly Evidence _evidence;
        private int _listDataCount;

        public int ListDataCount
        {
            get => _listDataCount;
            set
            {
                if (_listDataCount != value)
                {
                    _listDataCount = value;
                    NotifyOfPropertyChange();
                }
            }
        }

        public BindableCollection<string> ListOfContent { get; }

        public string ObjectId { get; set; }
        public bool PositiveFileRadio { get; set; }


        public ShowFileContentViewModel(Evidence evidence)
        {
            _evidence = evidence;
            ListOfContent = new BindableCollection<string>();
            ListDataCount = ListOfContent.Count;
            ObjectId = "0";
            PositiveFileRadio = false;
        }

        public void ShowPatientsFile()
        {
            ListOfContent.Clear();
            ListOfContent.AddRange(_evidence.GetPatientsFileContent());
            ListDataCount = ListOfContent.Count;
        }

        public void ShowTestsFile()
        {
            ListOfContent.Clear();
            ListOfContent.AddRange(_evidence.GetTestsFileContent());
            ListDataCount = ListOfContent.Count;
        }

        public void ShowDistrictsFile()
        {
            ListOfContent.Clear();
            ListOfContent.AddRange(_evidence.GetDistrictsFileContent());
            ListDataCount = ListOfContent.Count;
        }

        public void ShowRegionsFile()
        {
            ListOfContent.Clear();
            ListOfContent.AddRange(_evidence.GetRegionsFileContent());
            ListDataCount = ListOfContent.Count;
        }

        public void ShowWorkplacesFile()
        {
            ListOfContent.Clear();
            ListOfContent.AddRange(_evidence.GetWorkplacesFileContent());
            ListDataCount = ListOfContent.Count;
        }

        public void ShowDistrictFile()
        {
            ListOfContent.Clear();
            var content = _evidence.GetDistrictFileContent(int.Parse(ObjectId), PositiveFileRadio, out var found);

            if (found)
            {
                ListOfContent.AddRange(content);
                ListDataCount = ListOfContent.Count;
            }
            else
            {
                ShowNotFoundMessage();
            }
        }

        public void ShowRegionFile()
        {
            ListOfContent.Clear();
            var content = _evidence.GetRegionFileContent(int.Parse(ObjectId), PositiveFileRadio, out var found);

            if (found)
            {
                ListOfContent.AddRange(content);
                ListDataCount = ListOfContent.Count;
            }
            else
            {
                ShowNotFoundMessage();
            }
        }

        public void ShowPatientFile()
        {
            ListOfContent.Clear();
            var content = _evidence.GetPatientFileContent(ObjectId, out var found);

            if (found)
            {
                ListOfContent.AddRange(content);
                ListDataCount = ListOfContent.Count;
            }
            else
            {
                ShowNotFoundMessage();
            }
        }

        public void ShowWorkplaceFile()
        {
            ListOfContent.Clear();
            var content = _evidence.GetWorkplaceFileContent(int.Parse(ObjectId), out var found);

            if (found)
            {
                ListOfContent.AddRange(content);
                ListDataCount = ListOfContent.Count;
            }
            else
            {
                ShowNotFoundMessage();
            }
        }

        public void ShowNotFoundMessage()
        {
            MessageBox.Show("Hľadaný súbor neexistuje.", "Prehľadávanie súborov", MessageBoxButton.OK,
                MessageBoxImage.Warning);
        }
    }
}