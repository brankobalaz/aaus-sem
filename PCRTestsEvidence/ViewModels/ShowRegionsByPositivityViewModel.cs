﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowRegionsByPositivityViewModel : ShowObjectsByPositivityViewModelTemplate<ShellRegion>
    {
        public ShowRegionsByPositivityViewModel(Evidence evidence) : base("kraje", evidence)
        {
        }

        protected override List<ShellRegion> GetOrder()
        {
            var minDate = Date.AddDays(-QuarantineDays + 1).Date;
            var maxDate = Date.AddDays(1).Date;
            return _evidence.GetRegionsByPositivityInInterval(minDate, maxDate);
        }
    }
}
