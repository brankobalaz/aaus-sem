﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Caliburn.Micro;
using Microsoft.Xaml.Behaviors.Media;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowTestsByDistrictViewModel : ShowTestsByDateByPositivityViewModelTemplate<int>
    {

        public ShowTestsByDistrictViewModel(Evidence evidence) : base("Okres", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(int mode, out int err)
        {
            return _evidence.GetTestsInDistrictInInterval(Id, FromDate, ToDate, mode, out err);
        }
    }
}