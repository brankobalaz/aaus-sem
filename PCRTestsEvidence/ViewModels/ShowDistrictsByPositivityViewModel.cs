﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Microsoft.Xaml.Behaviors;
using PCRTestsEvidence.Models;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowDistrictsByPositivityViewModel : ShowObjectsByPositivityViewModelTemplate<ShellDistrict>
    {
        public ShowDistrictsByPositivityViewModel(Evidence evidence) : base("okresy", evidence)
        {
        }

        protected override List<ShellDistrict> GetOrder()
        {
            var minDate = Date.AddDays(-QuarantineDays + 1).Date;
            var maxDate = Date.AddDays(1).Date;
            return _evidence.GetDistrictsByPositivityInInterval(minDate, maxDate);
        }
    }
}