﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using PCRTestsEvidence.Models;
using PCRTestsEvidence.Views;

namespace PCRTestsEvidence.ViewModels
{
    public class ShowTestsByWorkplaceViewModel : ShowTestsByDateByPositivityViewModelTemplate<int>
    {
        public ShowTestsByWorkplaceViewModel(Evidence evidence) : base("Pracovisko", evidence)
        {
        }

        protected override List<ShellPCRTest> GetTests(int mode, out int err)
        {
            return _evidence.GetTestsInWorkplaceInInterval(Id, FromDate, ToDate, out err);
        }
    }
}