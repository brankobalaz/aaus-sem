﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace HeapFileTester
{
    public class Property : IRecord, IComparable<Property>, IComparable
    {
        private const int MAX_NAME_SIZE = 20;
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }

        public double DecimaValue { get; set; }

        protected bool Equals(Property other)
        {
            return Id == other.Id && Name == other.Name && Birthday.Equals(other.Birthday) && DecimaValue.Equals(other.DecimaValue);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Property) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Birthday, DecimaValue);
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Birthday)}: {Birthday}, {nameof(DecimaValue)}: {DecimaValue}";
        }

        public byte[] ToByteArray()
        {
            var idBytes = BitConverter.GetBytes(Id);
            var nameValidBytes = BitConverter.GetBytes(Name.Length);
            var tempName = Name.PadRight(MAX_NAME_SIZE, '*');
            var nameBytes = Name == null
                ? new byte[MAX_NAME_SIZE * sizeof(char)]
                : Encoding.Unicode.GetBytes(tempName.ToCharArray());
            var birthdayBytes = BitConverter.GetBytes(Birthday.Ticks);

            return idBytes.Concat(nameValidBytes).Concat(nameBytes).Concat(birthdayBytes).ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            var idBytes = array[..4];
            var nameValidBytes = array[4..8];
            var nameBytes = array[8..(2 * MAX_NAME_SIZE + 8)];
            var birthdayBytes = array[(2 * MAX_NAME_SIZE + 8)..];

            Id = BitConverter.ToInt32(idBytes);
            var nameValidLength = BitConverter.ToInt32(nameValidBytes);
            Name = Encoding.Unicode.GetString(nameBytes[..(2 * nameValidLength)]);
            var ticks = BitConverter.ToInt64(birthdayBytes);
            Birthday = DateTime.FromBinary(ticks);
        }

        public int ByteSize()
        {
            return 2 * sizeof(int) + MAX_NAME_SIZE * sizeof(char) + sizeof(long);
        }

        public int CompareTo(Property other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var idComparison = Id.CompareTo(other.Id);
            if (idComparison != 0) return idComparison;
            var nameComparison = string.Compare(Name, other.Name, StringComparison.Ordinal);
            if (nameComparison != 0) return nameComparison;
            return Birthday.CompareTo(other.Birthday);
        }

        public int CompareTo(object obj)
        {
            if (ReferenceEquals(null, obj)) return 1;
            if (ReferenceEquals(this, obj)) return 0;
            return obj is Property other ? CompareTo(other) : throw new ArgumentException($"Object must be of type {nameof(Property)}");
        }
    }
}