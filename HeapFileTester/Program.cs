﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using Bogus;
using HeapFile;

namespace HeapFileTester
{
    internal class Program
    {
        private const int OPERATIONS_IN_TEST_COUNT = 10000;
        private const bool REMOVE_ALL = false;

        static void Main(string[] args)
        {
            using var hFile = new HeapFile<Property>("ulozisko.data");
            var map = new Dictionary<long, Property>();
            var addresses = new List<long>();

            var start = DateTime.Now;

            Console.WriteLine($"Test 1 : Insert:Remove 50:50: na {OPERATIONS_IN_TEST_COUNT} prvkoch:");
            TestInsertRemove(hFile, map, addresses, 0.5);
            Console.WriteLine($"\nTest result is: {(TestOK(hFile, map, addresses) ? "OK" : "NG")}");

            Console.WriteLine($"\nTest 2 : Insert:Remove 80:20: na {OPERATIONS_IN_TEST_COUNT} prvkoch:");
            TestInsertRemove(hFile, map, addresses, 0.8);
            Console.WriteLine($"\nTest result is: {(TestOK(hFile, map, addresses) ? "OK" : "NG")}");

            if (REMOVE_ALL)
            {
                Console.WriteLine("\nTest 3 : Remove all known");
                RemoveAll(hFile, map, addresses);
                Console.WriteLine("Done.");
            }

            var elapsed = DateTime.Now - start;

            Console.WriteLine($"\nTrvanie vykonávania ({OPERATIONS_IN_TEST_COUNT} operácií v jednot. testoch): ");
            Console.WriteLine($"{elapsed.Minutes} min {elapsed.Seconds} s {elapsed.Milliseconds} ms");
            Console.WriteLine($"\nRecord size in Bytes: {new Property().ByteSize() + 1}");
        }

        private static void TestInsertRemove(HeapFile<Property> hFile, Dictionary<long, Property> map,
            List<long> addresses, double insertProbability)
        {
            var rand = new Random();
            var faker = new Faker("sk");

            for (int i = 1; i <= OPERATIONS_IN_TEST_COUNT; i++)
            {
                var r = rand.NextDouble();

                if (r < insertProbability || map.Count == 0) // INSERT
                {
                    var name = faker.Name.FirstName();
                    var date = faker.Date.Past(30);
                    var id = rand.Next();

                    var prop = new Property {Id = id, Name = name, Birthday = date};

                    var addr = hFile.Insert(prop);
                    map.Add(addr, prop);
                    addresses.Add(addr);
                }
                else // REMOVE
                {
                    var randomIndex = rand.Next(addresses.Count);
                    var randomAddr = addresses[randomIndex];

                    hFile.RemoveAt(randomAddr);
                    map.Remove(randomAddr);
                    addresses.Remove(randomAddr);
                }

                if ((i + 1) % 500 == 0)
                    WriteProgressBar(i, OPERATIONS_IN_TEST_COUNT);
            }
        }

        private static bool TestOK(HeapFile<Property> hFile, Dictionary<long, Property> map,
            List<long> addresses)
        {
            foreach (var addr in addresses)
            {
                var fromFile = hFile.GetAt(addr);
                var fromMap = map[addr];
                if (!fromFile.Equals(fromMap))
                {
                    Console.WriteLine(fromFile);
                    Console.WriteLine(fromMap);
                    return false;
                }
            }

            return true;
        }

        private static void RemoveAll(HeapFile<Property> hFile, Dictionary<long, Property> map,
            List<long> addresses)
        {
            foreach (var addr in addresses)
            {
                hFile.RemoveAt(addr);
                map.Remove(addr);
            }

            addresses.Clear();
        }

        private static void WriteProgressBar(int i, int max)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("[");
            var pivot = 20L * i / max;
            for (int j = 0; j < 20; j++)
            {
                if (j < pivot)
                    Console.Write("*");
                else
                    Console.Write(" ");
            }

            Console.Write("]");
        }
    }
}