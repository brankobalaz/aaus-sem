﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree23
{
    public class Tree23Node<T> : IComparable<Tree23Node<T>>, IComparable where T : IComparable<T>
    {
        public const int NH_SON = 0;
        public const int NH_DATA = 0;

        public const int N2_L_SON = 0;
        public const int N2_R_SON = 1;

        public const int N3_L_SON = 0;
        public const int N3_M_SON = 1;
        public const int N3_R_SON = 2;

        public const int N4_L_SON = 0;
        public const int N4_ML_SON = 1;
        public const int N4_MR_SON = 2;
        public const int N4_R_SON = 3;

        public const int N2_DATA = 0;

        public const int N3_L_DATA = 0;
        public const int N3_R_DATA = 1;

        public const int N4_L_DATA = 0;
        public const int N4_M_DATA = 1;
        public const int N4_R_DATA = 2;

        private Tree23Node<T> _parent;
        private readonly List<T> _data;
        private readonly List<Tree23Node<T>> _children;

        public Tree23Node(T data)
        {
            _data = new List<T>(3);
            _children = new List<Tree23Node<T>>(4);
            _data.Add(data);
        }

        public int CompareTo(Tree23Node<T> other)
        {
            return GetMax().CompareTo(other.GetMin());
        }

        public int CompareTo(object obj)
        {
            if (obj is null) return 1;
            if (ReferenceEquals(this, obj)) return 0;
            return obj is Tree23Node<T> other
                ? CompareTo(other)
                : throw new ArgumentException($"Object must be of type {nameof(Tree23Node<T>)}");
        }


        public bool IsRoot()
        {
            return _parent == null;
        }

        public bool IsLeaf()
        {
            return _children.Count == 0;
        }

        public bool IsHole()
        {
            return _data.Count == 0;
        }

        public bool Is2Node()
        {
            return _data.Count == 1;
        }

        public bool Is3Node()
        {
            return _data.Count == 2;
        }

        public bool Is4Node()
        {
            return _data.Count == 3;
        }

        public int Degree()
        {
            return _children.Count;
        }

        public Tree23Node<T> GetParent()
        {
            return _parent;
        }

        public void SetParent(Tree23Node<T> newParent)
        {
            _parent = newParent;
        }

        public Tree23Node<T> GetBrother()
        {
            if (GetParent() == null)
                throw new InvalidOperationException("Root does not have brother!");

            if (this == GetParent().GetLeftSon())
            {
                return GetParent().Is3Node() ? GetParent().GetMiddleSon() : GetParent().GetRightSon();
            }

            if (this == GetParent().GetRightSon())
            {
                return GetParent().Is3Node() ? GetParent().GetMiddleSon() : GetParent().GetLeftSon();
            }

            return GetParent().GetLeftSon();
        }

        public Tree23Node<T> GetSon(int sonIndex)
        {
            return _children[sonIndex];
        }

        public void InsertSon(Tree23Node<T> son)
        {
            son.SetParent(this);
            _children.Add(son);
            _children.Sort();
        }

        public void ResetParent()
        {
            _parent = null;
        }

        public bool RemoveSon(Tree23Node<T> node)
        {
            node.ResetParent();
            return _children.Remove(node);
        }

        public override string ToString()
        {
            if (IsHole())
                return $"H[ / | / ]";

            if (Is2Node())
                return $"2[ {AccessData(N2_DATA).ToString()} | / ]";

            return $"3[ {AccessData(N3_L_DATA).ToString()} | {AccessData(N3_R_DATA).ToString()} ]";
        }

        public T AccessData(int index)
        {
            return _data[index];
        }

        public bool ContainsData(T data)
        {
            if (Is2Node())
            {
                return data.Equals(_data[N2_DATA]);
            }

            if (Is3Node())
            {
                return data.Equals(_data[N3_L_DATA]) || data.Equals(_data[N3_R_DATA]);
            }

            return false;
        }

        public T GetData(T key)
        {
            return _data.Count switch
            {
                1 => _data[N2_DATA],
                2 => _data[N3_L_DATA].CompareTo(key) == 0 ? _data[N3_L_DATA] : _data[N3_R_DATA],
                _ => default,
            };
        }

        public T GetMin()
        {
            if (Is2Node())
                return _data[N2_DATA];

            return _data[N3_L_DATA].CompareTo(_data[N3_R_DATA]) > 0 ? _data[N3_R_DATA] : _data[N3_L_DATA];
        }

        public T GetMax()
        {
            if (Is2Node())
                return _data[N2_DATA];

            return _data[N3_L_DATA].CompareTo(_data[N3_R_DATA]) < 0 ? _data[N3_R_DATA] : _data[N3_L_DATA];
        }

        public bool HasLeftSon()
        {
            return Degree() != 0;
        }

        public bool HasRightSon()
        {
            return Degree() != 0;
        }

        public bool HasMiddleSon()
        {
            return Degree() == 3;
        }

        public bool HasMiddleLeftSon()
        {
            return HasMiddleSons();
        }

        public bool HasMiddleRightSon()
        {
            return HasMiddleSons();
        }

        private bool HasMiddleSons()
        {
            return Degree() == 4;
        }

        public Tree23Node<T> GetLeftSon()
        {
            return HasLeftSon() ? _children[NH_SON] : null;
        }

        public Tree23Node<T> GetMiddleSon()
        {
            return HasMiddleSon() ? _children[N3_M_SON] : null;
        }

        public Tree23Node<T> GetMiddleLeftSon()
        {
            return HasMiddleLeftSon() ? _children[N4_ML_SON] : null;
        }

        public Tree23Node<T> GetMiddleRightSon()
        {
            return HasMiddleRightSon() ? _children[N4_MR_SON] : null;
        }

        public bool HasOneSon()
        {
            return IsHole() && Degree() != 0;
        }

        public Tree23Node<T> GetOneSon()
        {
            return HasOneSon() ? _children[NH_SON] : null;
        }

        public Tree23Node<T> GetRightSon()
        {
            return _children.Count switch
            {
                1 => _children[NH_SON],
                2 => _children[N2_R_SON],
                3 => _children[N3_R_SON],
                4 => _children[N4_R_SON],
                _ => null,
            };
        }

        public T GetOneData()
        {
            if (Is2Node())
                return AccessData(N2_DATA);

            throw new InvalidOperationException("I'm not 2-node ...");
        }

        public T GetRightData()
        {
            return _data.Count switch
            {
                1 => _data[N2_DATA],
                2 => _data[N3_R_DATA],
                3 => _data[N4_R_DATA],
                _ => throw new InvalidOperationException("I have no data!"),
            };
        }

        public T GetLeftData()
        {
            return !IsHole() ? _data[N2_DATA] : throw new InvalidOperationException("I have no data!");
        }

        public T GetMiddleData()
        {
            return Is4Node() ? _data[N4_M_DATA] : throw new InvalidOperationException("I have no data in middle!");
        }

        public bool RemoveData(T data)
        {
            return _data.Remove(data);
        }

        public void InsertData(T data)
        {
            _data.Add(data);
            _data.Sort();
        }
    }
}