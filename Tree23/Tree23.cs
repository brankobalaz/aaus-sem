﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Tree23
{
    public class Tree23<T> : IEnumerable<T>, ICollection<T> where T : IComparable<T>
    {
        private Tree23Node<T> _root;
        public int Size { get; protected set; }

        public bool IsEmpty()
        {
            return _root == null;
        }

        private Tree23Node<T> GetRoot()
        {
            return _root;
        }

        public bool Insert(T data)
        {
            // Tree is empty
            if (_root == null)
            {
                _root = new Tree23Node<T>(data);
                Size++;
                return true;
            }

            // Tree is not empty

            var nodeToInsert = FindNode(data, out var found);

            if (found)
                return false; // Data is already in tree

            Size++;

            if (nodeToInsert.Is2Node()) // Leaf
            {
                nodeToInsert.InsertData(data);
            }
            else // if (nodeToInsert.Is3Node()) // Leaf
            {
                nodeToInsert.InsertData(data);
                ResolveInsert(nodeToInsert);
            }

            return true;
        }

        // Podklady: Toto šlo podľa prednášky a s ceruzkou
        // Is called only if parameter is 4-node
        private void ResolveInsert(Tree23Node<T> currentNode)
        {
            while (currentNode.Is4Node())
            {
                var newLeftNode = new Tree23Node<T>(currentNode.GetLeftData());
                var newRightNode = new Tree23Node<T>(currentNode.GetRightData());
                var middleVal = currentNode.GetMiddleData();

                if (!currentNode.IsLeaf())
                {
                    var leftSon = currentNode.GetLeftSon();
                    var middleLeftSon = currentNode.GetMiddleLeftSon();
                    var middleRightSon = currentNode.GetMiddleRightSon();
                    var rightSon = currentNode.GetRightSon();

                    newLeftNode.InsertSon(leftSon);
                    newLeftNode.InsertSon(middleLeftSon);
                    newRightNode.InsertSon(middleRightSon);
                    newRightNode.InsertSon(rightSon);
                }

                Tree23Node<T> parent;

                if (currentNode.IsRoot())
                {
                    parent = new Tree23Node<T>(middleVal);
                    SetRoot(parent);
                }
                else
                {
                    parent = currentNode.GetParent();
                    parent.RemoveSon(currentNode);
                    parent.InsertData(middleVal);
                }

                parent.InsertSon(newLeftNode);
                parent.InsertSon(newRightNode);

                currentNode = parent;
            }
        }

        private void SetRoot(Tree23Node<T> node)
        {
            _root = node;
        }

        private void ResetRoot()
        {
            _root = null;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var i = 0;
            foreach (var data in this)
            {
                array[arrayIndex + i] = data;
                i++;
            }
        }

        public bool Remove(T dataToRemove)
        {
            var currentNode = FindNode(dataToRemove, out var found);

            if (!found)
                return false;

            Size--;

            ResolveRemove(dataToRemove, currentNode);
            return true;
        }

        public int Count
        {
            get => Size;
        }

        public bool IsReadOnly { get; }

        // Podklady:
        // Je to implementované podľa prednášky, ale pseudoalgoritmus z prednášky mi docvakol až po prečítaní tohto šťavnatého článku:
        // https://www.cs.princeton.edu/~dpw/courses/cos326-12/ass/2-3-trees.pdf
        // Predtým ako som použil google som sa to snažil asi hodinu rozkresliť podľa prednášky sám, ale nikam som sa nedostal...
        private void ResolveRemove(T dataToRemove, Tree23Node<T> currentNode)
        {
            // Data in currentNode will be replaced by InOrderSuccessor and we will continue from leaf (where we take data from)
            if (!currentNode.IsLeaf())
            {
                var inOrderSuccessorData =
                    GetInOrderSuccessor(dataToRemove, currentNode, out var inOrderSuccessorNode, out _);

                currentNode.RemoveData(dataToRemove);
                inOrderSuccessorNode.RemoveData(inOrderSuccessorData);
                currentNode.InsertData(inOrderSuccessorData);

                currentNode = inOrderSuccessorNode;
            }
            else
            {
                currentNode.RemoveData(dataToRemove);
            }

            if (currentNode.Is2Node())
                return;

            while (currentNode.IsHole())
            {
                if (currentNode.IsRoot())
                {
                    if (currentNode.IsLeaf())
                    {
                        ResetRoot();
                        break;
                    }

                    var newRoot = currentNode.GetOneSon();
                    currentNode.RemoveSon(newRoot);
                    SetRoot(newRoot);
                    break;
                }

                var brother = currentNode.GetBrother();
                var parent = currentNode.GetParent();
                var parentData = GetCommonDataFromParent(currentNode, brother);

                parent.RemoveData(parentData);

                if (brother.Is2Node())
                {
                    parent.RemoveSon(currentNode);
                    brother.InsertData(parentData);

                    if (!currentNode.IsLeaf())
                    {
                        var currentNodeSon = currentNode.GetOneSon();
                        currentNode.RemoveSon(currentNodeSon);
                        brother.InsertSon(currentNodeSon);
                    }

                    currentNode = parent;
                }
                else //if(brother.Is3Node())
                {
                    currentNode.InsertData(parentData);

                    var brotherData =
                        GetNearestDataFrom3NodeBrother(currentNode, brother, out var sonToChangeParent);

                    brother.RemoveData(brotherData);
                    parent.InsertData(brotherData);

                    if (!brother.IsLeaf())
                    {
                        brother.RemoveSon(sonToChangeParent);
                        currentNode.InsertSon(sonToChangeParent);
                    }
                }
            }
        }

        private T GetNearestDataFrom3NodeBrother(Tree23Node<T> nodeA, Tree23Node<T> nodeB,
            out Tree23Node<T> sonToChangeParent)
        {
            var parent = nodeA.GetParent();
            var node3 = nodeA.Is3Node() ? nodeA : nodeB;
            var other = !nodeA.Is3Node() ? nodeA : nodeB;

            if (node3 == parent.GetLeftSon())
            {
                sonToChangeParent = node3.GetRightSon();
                return node3.GetRightData();
            }

            if (node3 == parent.GetRightSon())
            {
                sonToChangeParent = node3.GetLeftSon();
                return node3.GetLeftData();
            }

            if (other == parent.GetLeftSon())
            {
                sonToChangeParent = node3.GetLeftSon();
                return node3.GetLeftData();
            }

            // if (other == parent.GetRightSon())
            sonToChangeParent = node3.GetRightSon();
            return node3.GetRightData();
        }

        private T GetCommonDataFromParent(Tree23Node<T> nodeA, Tree23Node<T> nodeB)
        {
            var parent = nodeA.GetParent();

            if (parent.Is2Node())
            {
                return parent.GetOneData();
            }

            var hole = nodeA.IsHole() ? nodeA : nodeB;
            var node = !nodeA.IsHole() ? nodeA : nodeB;

            if (hole == parent.GetLeftSon())
                return parent.GetLeftData();

            if (hole == parent.GetRightSon())
                return parent.GetRightData();

            return node == parent.GetLeftSon() ? parent.GetLeftData() : parent.GetRightData();
        }

        void ICollection<T>.Clear()
        {
            Clear();
        }

        public bool Contains(T data)
        {
            FindNode(data, out var found);
            return found;
        }

        // Podklady: Podľa prednášok, niečo si pamätám z ešte z auš 1 - tam bolo BST
        private Tree23Node<T> FindNode(T data, out bool found)
        {
            var result = GetRoot();

            if (result == null)
            {
                found = false;
                return null;
            }

            while (!result.ContainsData(data) && !result.IsLeaf())
            {
                if (data.CompareTo(result.GetMin()) < 0)
                {
                    result = result.GetLeftSon();
                }
                else if (data.CompareTo(result.GetMax()) > 0)
                {
                    result = result.GetRightSon();
                }
                else
                {
                    result = result.GetMiddleSon();
                }
            }

            found = result.ContainsData(data);
            return result;
        }

        // In-order
        // Podklady: https://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
        public IEnumerator<T> GetEnumerator()
        {
            var current = GetRoot();

            if (current == null)
                yield break;

            var stack = new Stack<(Tree23Node<T>, bool)>();
            stack.Push((current, false));

            while (stack.Count > 0)
            {
                while (!current.IsLeaf())
                {
                    current = current.GetLeftSon();
                    stack.Push((current, false));
                }

                bool visited;
                (current, visited) = stack.Pop();

                if (current.Is2Node())
                {
                    yield return current.AccessData(Tree23Node<T>.N2_DATA);
                }
                else if (current.Is3Node() && !visited)
                {
                    yield return current.AccessData(Tree23Node<T>.N3_L_DATA);
                    stack.Push((current, true));
                }
                else // if (current.Is3Node() && visited)
                {
                    yield return current.AccessData(Tree23Node<T>.N3_R_DATA);
                }

                if (current.IsLeaf())
                    continue;

                if (current.Is3Node() && !visited)
                {
                    current = current.GetMiddleSon();
                    stack.Push((current, false));
                }
                else
                {
                    current = current.GetRightSon();
                    stack.Push((current, false));
                }
            }
        }

        public override string ToString()
        {
            var result = "";
            var queueNext = new Queue<Tree23Node<T>>();

            if (_root == null)
                return "Tree is empty.";

            queueNext.Enqueue(_root);

            while (queueNext.Count > 0)
            {
                var queueCurrent = new Queue<Tree23Node<T>>(queueNext);
                queueNext.Clear();

                while (queueCurrent.Count > 0)
                {
                    var node = queueCurrent.Dequeue();
                    result += node.ToString();

                    result += " ";

                    for (var i = 0; i < node.Degree(); i++)
                    {
                        queueNext.Enqueue(node.GetSon(i));
                    }
                }

                result += '\n';
            }

            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        // Podklady: https://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
        static T GetInOrderSuccessor(T data, Tree23Node<T> inNode, out Tree23Node<T> outNode, out bool hasSuccessor)
        {
            hasSuccessor = true;

            if (data.CompareTo(inNode.GetLeftData()) < 0)
            {
                outNode = inNode;
                return outNode.GetLeftData();
            }

            if (!inNode.IsLeaf())
            {
                // S tým || by to podľa mojej skromnej mienky mohlo byť rýchlejšie - ak to vyjde true pri Equals, tak nebude robiť tie dva compareTo
                if (inNode.Is3Node() && (inNode.GetLeftData().Equals(data) ||
                                         inNode.GetLeftData().CompareTo(data) < 0 &&
                                         data.CompareTo(inNode.GetRightData()) < 0))
                {
                    outNode = GetLeftmostNodeFromSubtree(inNode.GetMiddleSon());
                }
                else
                {
                    outNode = GetLeftmostNodeFromSubtree(inNode.GetRightSon());
                }

                return outNode.GetLeftData();
            }

            if (inNode.Is3Node() && (inNode.GetLeftData().Equals(data) ||
                                     inNode.GetLeftData().CompareTo(data) < 0 &&
                                     data.CompareTo(inNode.GetRightData()) < 0))
            {
                outNode = inNode;
                return inNode.GetRightData();
            }

            var parent = inNode.GetParent();
            var current = inNode;

            // idem hore, až kým neprídem do rodiča zľava
            while (parent != null && current == parent.GetRightSon())
            {
                current = parent;
                parent = parent.GetParent();
            }

            outNode = parent;

            if (parent == null)
            {
                hasSuccessor = false;
                return default;
            }

            if (parent.Is3Node() && current == parent.GetMiddleSon())
                return parent.GetRightData();

            return parent.GetLeftData();
        }

        private static Tree23Node<T> GetLeftmostNodeFromSubtree(Tree23Node<T> node)
        {
            while (node.HasLeftSon())
            {
                node = node.GetLeftSon();
            }

            return node;
        }

        public bool TestBalance()
        {
            var treeLevel = GetHeightOfSubtree(GetRoot());

            foreach (var node in GetNodeEnumerator())
            {
                if (node.IsLeaf() && GetLevelsToRoot(node) != treeLevel)
                    return false;
            }

            return true;
        }

        private int GetLevelsToRoot(Tree23Node<T> node)
        {
            int result = 0;
            while (node != GetRoot())
            {
                node = node.GetParent();
                result++;
            }

            return result;
        }

        // Level-order
        // Podklady: Toto šlo nejak bez podkadov
        public IEnumerable<Tree23Node<T>> GetNodeEnumerator()
        {
            if (GetRoot() == null)
                yield break;

            var queue = new Queue<Tree23Node<T>>();

            queue.Enqueue(GetRoot());

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                yield return node;

                for (var i = 0; i < node.Degree(); i++)
                    queue.Enqueue(node.GetSon(i));
            }
        }

        private int GetHeightOfSubtree(Tree23Node<T> node)
        {
            var result = 0;
            while (node.HasLeftSon())
            {
                node = node.GetLeftSon();
                result++;
            }

            return result;
        }

        public T Find(T key, out bool wasFound)
        {
            var node = FindNode(key, out var found);
            wasFound = found;
            return found == false ? default : node.GetData(key);
        }

        // Podklady: Využité poznatky z už využitých podkladov a cvičenia v T5
        public IEnumerable<T> GetIntervalEnumerator(T min, T max)
        {
            if (IsEmpty())
                yield break;

            if (min.CompareTo(max) > 0)
                yield break;

            var currentNode = FindNode(min, out var found);
            var currentData = min;
            var successorExists = true;

            if (!found)
                currentData = GetInOrderSuccessor(min, currentNode, out currentNode, out successorExists);

            while (successorExists && currentData.CompareTo(max) <= 0)
            {
                yield return currentData;
                currentData = GetInOrderSuccessor(currentData, currentNode, out currentNode, out successorExists);
            }
        }

        public void Add(T item)
        {
            Insert(item);
        }

        public int Clear()
        {
            var removed = 0;

            while (!IsEmpty())
            {
                Remove(GetRoot().GetMin());
                removed++;
            }

            return removed;
        }
    }
}