﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BTree3;
using HeapFile;

namespace BTree3
{
    public class BTree3<T> : IDisposable, IEnumerable<T>, ICollection<T>
        where T : IRecord, IComparable<T>, new()
    {
        public const int NOT_FOUND = -1;

        public const int FROM_LEFT = 0;
        public const int FROM_RIGHT = 1;

        private long _root;
        private readonly BTreeFile<T> _file;
        private int _size;

        public int Size
        {
            get => _size;
            protected set
            {
                if (_size != value)
                {
                    _size = value;
                    _file.Size = _size;
                }
            }
        }

        public BTree3(string fileName)
        {
            _file = new BTreeFile<T>(fileName);
            SetRootAddr(_file.Root);
            Size = _file.Size;
        }

        public bool IsEmpty()
        {
            return _root == NOT_FOUND;
        }

        private BTree3Node<T> GetRoot()
        {
            if (_root == NOT_FOUND)
                return null;
            else
                return LoadNode(_root);
        }

        private long GetRootAddress()
        {
            return _root;
        }

        public bool Insert(T data)
        {
            // Tree is empty
            if (IsEmpty())
            {
                var newRoot = new BTree3Node<T>(data);
                newRoot.ResetParent();
                var newRootAddr = SaveNode(newRoot);
                SetRootAddr(newRootAddr);
                Size++;
                return true;
            }

            // Tree is not empty

            var nodeToInsert = FindNode(data, out var found, out var nodeAddr);

            if (found)
            {
                nodeToInsert.InsertData(data);
                SaveNode(nodeAddr, nodeToInsert);
                return true;
            }

            Size++;

            if (nodeToInsert.Is2Node()) // Leaf
            {
                nodeToInsert.InsertData(data);
                SaveNode(nodeAddr, nodeToInsert);
            }
            else // if (nodeToInsert.Is3Node()) // Leaf
            {
                nodeToInsert.InsertData(data);
                ResolveInsert(nodeToInsert, nodeAddr);
            }

            return true;
        }

        public LinkedList<string> GetFileContent()
        {
            return _file.GetFileContent();
        }

        // Podklady: Toto šlo podľa prednášky a s ceruzkou
        // Is called only if parameter is 4-node
        private void ResolveInsert(BTree3Node<T> currentNode, long currentAddr)
        {
            while (currentNode.Is4Node())
            {
                var newLeftNode = new BTree3Node<T>(currentNode.GetLeftData());
                var newRightNode = new BTree3Node<T>(currentNode.GetRightData());
                var middleData = currentNode.GetMiddleData();

                if (!currentNode.IsLeaf())
                {
                    var leftSonAddr = currentNode.GetLeftSon();
                    var middleLeftSonAddr = currentNode.GetMiddleLeftSon();
                    var middleRightSonAddr = currentNode.GetMiddleRightSon();
                    var rightSonAddr = currentNode.GetRightSon();

                    newLeftNode.InsertLeftSon(leftSonAddr);
                    newLeftNode.InsertRightSon(middleLeftSonAddr);
                    newRightNode.InsertLeftSon(middleRightSonAddr);
                    newRightNode.InsertRightSon(rightSonAddr);
                }

                BTree3Node<T> parent;
                long parentAddr;
                var removedSonIndex = 0; // ak root - tak vkladám lavý, pravý, ak nie root, vkladám podľa priradenia

                if (currentNode.IsRoot())
                {
                    parent = new BTree3Node<T>(middleData);
                    parentAddr = SaveNode(parent);
                    SetRoot(parentAddr, parent);
                }
                else
                {
                    parentAddr = currentNode.GetParent();
                    parent = LoadNode(parentAddr);
                    removedSonIndex = parent.RemoveSon(currentNode, currentAddr);
                    parent.InsertData(middleData);
                }

                newLeftNode.SetParent(parentAddr);
                newRightNode.SetParent(parentAddr);

                // prepíšem záznam, ktorý by som mazal - ušetrím jeden prístup do pamäte
                SaveNode(currentAddr, newLeftNode); 

                var newLeftNodeAddr = currentAddr;
                var newRightNodeAddr = SaveNode(newRightNode);

                if (!currentNode.IsLeaf())
                {
                    var leftSonAddr = currentNode.GetLeftSon();
                    var middleLeftSonAddr = currentNode.GetMiddleLeftSon();
                    var middleRightSonAddr = currentNode.GetMiddleRightSon();
                    var rightSonAddr = currentNode.GetRightSon();

                    var leftSon = LoadNode(leftSonAddr);
                    leftSon.SetParent(newLeftNodeAddr);
                    SaveNode(leftSonAddr, leftSon);

                    var middleLeftSon = LoadNode(middleLeftSonAddr);
                    middleLeftSon.SetParent(newLeftNodeAddr);
                    SaveNode(middleLeftSonAddr, middleLeftSon);

                    var middleRightSon = LoadNode(middleRightSonAddr);
                    middleRightSon.SetParent(newRightNodeAddr);
                    SaveNode(middleRightSonAddr, middleRightSon);

                    var rightSon = LoadNode(rightSonAddr);
                    rightSon.SetParent(newRightNodeAddr);
                    SaveNode(rightSonAddr, rightSon);
                }

                parent.InsertSonAt(newLeftNodeAddr, removedSonIndex);
                parent.InsertSonAt(newRightNodeAddr, removedSonIndex + 1);

                currentNode = parent;
                currentAddr = parentAddr;
            }

            SaveNode(currentAddr, currentNode);
        }

        private void SetRootAddr(long nodeAddr)
        {
            _root = nodeAddr;
            _file.Root = nodeAddr;
        }

        private void SetRoot(long nodeAddr, BTree3Node<T> node)
        {
            node.ResetParent();
            SetRootAddr(nodeAddr);
        }

        private void ResetRoot()
        {
            SetRootAddr(NOT_FOUND);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var i = 0;
            foreach (var data in this)
            {
                array[arrayIndex + i] = data;
                i++;
            }
        }

        public bool Remove(T data)
        {
            var currentNode = FindNode(data, out var found, out var nodeAddr);

            if (!found)
                return false;

            Size--;

            ResolveRemove(data, currentNode, nodeAddr);
            return true;
        }

        public int Count => Size;

        public bool IsReadOnly => false;

        // Podklady:
        // Je to implementované podľa prednášky, ale pseudoalgoritmus z prednášky mi docvakol až po prečítaní tohto šťavnatého článku:
        // https://www.cs.princeton.edu/~dpw/courses/cos326-12/ass/2-3-trees.pdf
        // Predtým ako som použil google som sa to snažil asi hodinu rozkresliť podľa prednášky sám, ale nikam som sa nedostal...
        private void ResolveRemove(T dataToRemove, BTree3Node<T> currentNode, long currentAddr)
        {
            // Data in currentNode will be replaced by InOrderSuccessor and we will continue from leaf (where we take data from)
            if (!currentNode.IsLeaf())
            {
                var inOrderSuccessorData =
                    GetInOrderSuccessor(dataToRemove, currentNode, currentAddr, out var inOrderSuccessorNode,
                        out var inOrderSuccNodeArrd, out _);

                currentNode.RemoveData(dataToRemove);
                inOrderSuccessorNode.RemoveData(inOrderSuccessorData);
                currentNode.InsertData(inOrderSuccessorData);

                SaveNode(currentAddr, currentNode);

                currentNode = inOrderSuccessorNode;
                currentAddr = inOrderSuccNodeArrd;
            }
            else
            {
                currentNode.RemoveData(dataToRemove);
            }

            if (currentNode.Is2Node())
            {
                SaveNode(currentAddr, currentNode);
                return;
            }

            while (currentNode.IsHole())
            {
                if (currentNode.IsRoot())
                {
                    if (currentNode.IsLeaf())
                    {
                        ResetRoot();

                        DeleteNode(currentAddr);
                        return;
                    }

                    var newRootAddr = currentNode.GetOneSon();
                    var newRoot = LoadNode(newRootAddr);

                    SetRoot(newRootAddr, newRoot);
                    SaveNode(newRootAddr, newRoot);

                    DeleteNode(currentAddr);
                    return;
                }

                var parentAddr = currentNode.GetParent();
                var parent = LoadNode(parentAddr);

                var brotherAddr = currentNode.GetBrother(parent, currentAddr, out var brotherFrom);
                var brother = LoadNode(brotherAddr);

                var parentData = GetCommonDataFromParent(currentNode, currentAddr, brotherAddr, parent);

                parent.RemoveData(parentData);

                if (brother.Is2Node())
                {
                    parent.RemoveSon(currentNode, currentAddr);
                    brother.InsertData(parentData);

                    if (!currentNode.IsLeaf())
                    {
                        var currentNodeSonAddr = currentNode.GetOneSon();

                        if (brotherFrom == FROM_RIGHT)
                            brother.InsertLeftSon(currentNodeSonAddr);
                        else // if (brotherFrom == BTree3Node<K, D>.FROM_LEFT)
                            brother.InsertRightSon(currentNodeSonAddr);

                        var currentNodeSon = LoadNode(currentNodeSonAddr);
                        currentNodeSon.SetParent(brotherAddr);
                        SaveNode(currentNodeSonAddr, currentNodeSon);
                    }

                    DeleteNode(currentAddr);
                    currentNode = parent;
                    currentAddr = parentAddr;
                }
                else //if(brother.Is3Node())
                {
                    currentNode.InsertData(parentData);

                    var brotherData =
                        GetNearestDataFrom3NodeBrother(currentNode, currentAddr, brother, brotherAddr, parent,
                            out var sonToChangePrentAddr);

                    brother.RemoveData(brotherData);
                    parent.InsertData(brotherData);

                    if (!brother.IsLeaf())
                    {
                        if (brotherFrom == FROM_RIGHT)
                            currentNode.InsertRightSon(sonToChangePrentAddr);
                        else // if (brotherFrom == BTree3Node<K, D>.FROM_LEFT)
                            currentNode.InsertLeftSon(sonToChangePrentAddr);

                        var sonToChangeParent = LoadNode(sonToChangePrentAddr);

                        brother.RemoveSon(sonToChangeParent, sonToChangePrentAddr);
                        sonToChangeParent.SetParent(currentAddr);
                        SaveNode(sonToChangePrentAddr, sonToChangeParent);
                    }

                    SaveNode(parentAddr, parent);
                }

                SaveNode(brotherAddr, brother);
            }

            SaveNode(currentAddr, currentNode);
        }

        private T GetNearestDataFrom3NodeBrother(BTree3Node<T> nodeA, long nodeAAddr,
            BTree3Node<T> nodeB, long nodeBAddr, BTree3Node<T> parent, out long sonToChangeParentAddr)
        {
            var node3Addr = nodeA.Is3Node() ? nodeAAddr : nodeBAddr;
            var node3 = nodeA.Is3Node() ? nodeA : nodeB;

            var otherAddr = !nodeA.Is3Node() ? nodeAAddr : nodeBAddr;
            var other = !nodeA.Is3Node() ? nodeA : nodeB;

            if (node3Addr == parent.GetLeftSon())
            {
                sonToChangeParentAddr = node3.GetRightSon();
                return node3.GetRightData();
            }

            if (node3Addr == parent.GetRightSon())
            {
                sonToChangeParentAddr = node3.GetLeftSon();
                return node3.GetLeftData();
            }

            if (otherAddr == parent.GetLeftSon())
            {
                sonToChangeParentAddr = node3.GetLeftSon();
                return node3.GetLeftData();
            }

            // if (other == parent.GetRightSon())
            sonToChangeParentAddr = node3.GetRightSon();
            return node3.GetRightData();
        }

        private T GetCommonDataFromParent(BTree3Node<T> nodeA, long nodeAAddr, long nodeBAddr,
            BTree3Node<T> parent)
        {
            if (parent.Is2Node())
            {
                return parent.GetOneData();
            }

            var holeAddr = nodeA.IsHole() ? nodeAAddr : nodeBAddr;
            var nodeAddr = !nodeA.IsHole() ? nodeAAddr : nodeBAddr;

            if (holeAddr == parent.GetLeftSon())
                return parent.GetLeftData();

            if (holeAddr == parent.GetRightSon())
                return parent.GetRightData();

            return nodeAddr == parent.GetLeftSon() ? parent.GetLeftData() : parent.GetRightData();
        }

        public bool Contains(T data)
        {
            FindNode(data, out var found, out _);
            return found;
        }

        // Podklady: Podľa prednášok, niečo si pamätám z ešte z auš 1 - tam bolo BST
        private BTree3Node<T> FindNode(T data, out bool found, out long nodeAddress)
        {
            var result = GetRoot();
            nodeAddress = GetRootAddress();

            if (result == null)
            {
                found = false;
                nodeAddress = NOT_FOUND;
                return null;
            }

            while (!result.ContainsData(data) && !result.IsLeaf())
            {
                if (data.CompareTo(result.GetLeftData()) < 0)
                {
                    nodeAddress = result.GetLeftSon();
                }
                else if (data.CompareTo(result.GetRightData()) > 0)
                {
                    nodeAddress = result.GetRightSon();
                }
                else
                {
                    nodeAddress = result.GetMiddleSon();
                }

                result = LoadNode(nodeAddress);
            }

            found = result.ContainsData(data);
            return result;
        }

        // In-order
        // Podklady: https://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
        public IEnumerator<T> GetEnumerator()
        {
            var current = GetRoot();

            if (current == null)
                yield break;

            var stack = new Stack<(BTree3Node<T>, bool)>();
            stack.Push((current, false));

            while (stack.Count > 0)
            {
                while (!current.IsLeaf())
                {
                    current = LoadNode(current.GetLeftSon());
                    stack.Push((current, false));
                }

                bool visited;
                (current, visited) = stack.Pop();

                if (current.Is2Node())
                {
                    yield return current.GetOneData();
                }
                else if (current.Is3Node() && !visited)
                {
                    yield return current.GetLeftData();
                    stack.Push((current, true));
                }
                else // if (current.Is3Node() && visited)
                {
                    yield return current.GetRightData();
                }

                if (current.IsLeaf())
                    continue;

                if (current.Is3Node() && !visited)
                {
                    current = LoadNode(current.GetMiddleSon());
                    stack.Push((current, false));
                }
                else
                {
                    current = LoadNode(current.GetRightSon());
                    stack.Push((current, false));
                }
            }
        }
        
        public override string ToString()
        {
            var result = "";
            var queueNext = new Queue<(BTree3Node<T> Node, long Addr)>();

            if (_root == NOT_FOUND)
                return "Tree is empty.";

            queueNext.Enqueue((GetRoot(), GetRootAddress()));

            while (queueNext.Count > 0)
            {
                var queueCurrent = new Queue<(BTree3Node<T> Node, long Addr)>(queueNext);
                queueNext.Clear();

                while (queueCurrent.Count > 0)
                {
                    var node = queueCurrent.Dequeue();
                    result += node.Node.ToString() + $"@{node.Addr}";

                    result += " ";

                    for (var i = 0; i < node.Node.Degree(); i++)
                    {
                        queueNext.Enqueue((LoadNode(node.Node.GetSonAt(i)), node.Node.GetSonAt(i)));
                    }
                }

                result += '\n';
            }

            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        // Podklady: https://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
        private T GetInOrderSuccessor(T data, BTree3Node<T> inNode, long inNodeAddr,
            out BTree3Node<T> outNode, out long outNodeAddr, out bool hasSuccessor)
        {
            hasSuccessor = true;

            if (data.CompareTo(inNode.GetLeftData()) < 0)
            {
                outNode = inNode;
                outNodeAddr = inNodeAddr;
                return outNode.GetLeftData();
            }

            if (!inNode.IsLeaf())
            {
                // S tým || by to podľa mojej skromnej mienky mohlo byť rýchlejšie - ak to vyjde true pri Equals, tak nebude robiť tie dva compareTo
                if (inNode.Is3Node() && (inNode.GetLeftData().CompareTo(data) == 0 ||
                                         inNode.GetLeftData().CompareTo(data) < 0 &&
                                         data.CompareTo(inNode.GetRightData()) < 0))
                {
                    outNode = GetLeftmostNodeFromSubtree(LoadNode(inNode.GetMiddleSon()), inNode.GetMiddleSon(),
                        out var nodeAddress);
                    outNodeAddr = nodeAddress;
                }
                else
                {
                    outNode = GetLeftmostNodeFromSubtree(LoadNode(inNode.GetRightSon()), inNode.GetRightSon(),
                        out var nodeAddress);
                    outNodeAddr = nodeAddress;
                }

                return outNode.GetLeftData();
            }

            if (inNode.Is3Node() && (inNode.GetLeftData().CompareTo(data) == 0 ||
                                     inNode.GetLeftData().CompareTo(data) < 0 &&
                                     data.CompareTo(inNode.GetRightData()) < 0))
            {
                outNode = inNode;
                outNodeAddr = inNodeAddr;
                return inNode.GetRightData();
            }

            var parentAddr = inNode.GetParent();

            if (parentAddr == NOT_FOUND)
            {
                outNode = default;
                outNodeAddr = NOT_FOUND;
                hasSuccessor = false;
                return default;
            }

            var parent = LoadNode(parentAddr);
            var currentAddr = inNodeAddr;

            // idem hore, až kým neprídem do rodiča zľava
            while (parentAddr != NOT_FOUND && currentAddr == parent.GetRightSon())
            {
                currentAddr = parentAddr;
                parentAddr = parent.GetParent();
                parent = parentAddr != NOT_FOUND ? LoadNode(parentAddr) : null;
            }

            outNode = parent;
            outNodeAddr = parentAddr;

            if (outNodeAddr == NOT_FOUND)
            {
                hasSuccessor = false;
                return default;
            }

            if (outNode.Is3Node() &&
                currentAddr == outNode.GetMiddleSon()) // if pred týmto zabezpečí, že outNode nebude null
                return outNode.GetRightData();

            return outNode.GetLeftData();
        }

        private BTree3Node<T> GetLeftmostNodeFromSubtree(BTree3Node<T> node, long nodeAddr, out long nodeAddress)
        {
            nodeAddress = nodeAddr;

            while (!node.IsLeaf())
            {
                nodeAddress = node.GetLeftSon();
                node = LoadNode(nodeAddress);
            }

            return node;
        }

        public bool TestBalance()
        {
            var treeLevel = GetHeightOfSubtree(GetRoot());

            foreach (var node in GetNodeEnumerator())
            {
                if (node.Node.IsLeaf() && GetLevelsToRoot(node.Address) != treeLevel)
                    return false;
            }

            return true;
        }

        private int GetLevelsToRoot(long nodeAddr)
        {
            int result = 0;
            while (nodeAddr != GetRootAddress())
            {
                nodeAddr = LoadNode(nodeAddr).GetParent();
                result++;
            }

            return result;
        }

        // Level-order
        // Podklady: Toto šlo nejak bez podkadov
        public IEnumerable<(BTree3Node<T> Node, long Address)> GetNodeEnumerator()
        {
            var root = GetRoot();
            if (root == null)
                yield break;

            var queue = new Queue<(BTree3Node<T> Node, long Address)>();

            queue.Enqueue((root, GetRootAddress()));

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                yield return node;

                //var node = LoadNode(nodeAddr);
                for (var i = 0; i < node.Node.Degree(); i++)
                    queue.Enqueue((LoadNode(node.Node.GetSonAt(i)), node.Node.GetSonAt(i)));
            }
        }

        private int GetHeightOfSubtree(BTree3Node<T> node)
        {
            var result = 0;
            while (!node.IsLeaf())
            {
                node = LoadNode(node.GetLeftSon());
                result++;
            }

            return result;
        }

        public T Find(T data, out bool wasFound)
        {
            var node = FindNode(data, out var found, out _);
            wasFound = found;
            return found == false ? default : node.GetData(data);
        }

        // Podklady: Využité poznatky z už využitých podkladov a cvičenia v T5
        public IEnumerable<T> GetIntervalEnumerator(T min, T max)
        {
            if (IsEmpty())
                yield break;

            if (min.CompareTo(max) > 0)
                yield break;

            var currentNode = FindNode(min, out var found, out var currentAddr);
            var successorExists = true;

            T currentData = found
                ? currentNode.GetData(min)
                : GetInOrderSuccessor(min, currentNode, currentAddr, out currentNode, out currentAddr,
                    out successorExists);

            while (successorExists && currentData.CompareTo(max) <= 0)
            {
                yield return currentData;
                currentData = GetInOrderSuccessor(currentData, currentNode, currentAddr, out currentNode,
                    out currentAddr, out successorExists);
            }
        }

        public void Add(T item)
        {
            Insert(item);
        }

        void ICollection<T>.Clear()
        {
            Clear();
        }

        public int Clear()
        {
            var removed = Size;

            _file.Clear();

            Size = 0;
            SetRootAddr(-1);

            return removed;
        }

        private BTree3Node<T> LoadNode(long addr)
        {
            return _file.GetAt(addr);
        }

        private long SaveNode(BTree3Node<T> node)
        {
            return _file.Insert(node);
        }

        private bool SaveNode(long addr, BTree3Node<T> node)
        {
            return _file.UpdateAt(addr, node);
        }

        private bool DeleteNode(long addr)
        {
            return _file.RemoveAt(addr);
        }

        public void Dispose()
        {
            _file?.Dispose();
        }
    }
}