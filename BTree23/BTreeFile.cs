﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace BTree3
{
    public class BTreeFile<T> : HeapFile<BTree3Node<T>>
        where T : IRecord, IComparable<T>, new()
    {
        private const int NOT_FOUND = -1;
        public long Root { get; set; }
        public int Size { get; set; }

        public BTreeFile(string dataFileName) : base(dataFileName)
        {
            var length = _dataFileStream.Length;

            if (WasSavedProperly)
            {
                var footerAddr = length - sizeof(long) - sizeof(int);
                _dataFileStream.Seek(footerAddr, SeekOrigin.Begin);

                var footerBytes = new byte[sizeof(long) + sizeof(int)];
                _dataFileStream.Read(footerBytes, 0, sizeof(long) + sizeof(int));

                Root = BitConverter.ToInt64(footerBytes[..sizeof(long)]);
                Size = BitConverter.ToInt32(footerBytes[sizeof(long)..]);

                _dataFileStream.SetLength(footerAddr);
            }
            else
            {
                Root = NOT_FOUND;
                Size = 0;

                if (WasSaved)
                    ReconstructRootAndSize();
            }
        }

        public override void Dispose()
        {
            var footerBytes = new byte[] { };
            footerBytes = footerBytes
                .Concat(BitConverter.GetBytes(Root))
                .Concat(BitConverter.GetBytes(Size))
                .ToArray();

            var footerAddr = _dataFileStream.Length;
            _dataFileStream.Seek(footerAddr, SeekOrigin.Begin);
            _dataFileStream.Write(footerBytes);

            base.Dispose();
        }

        private void ReconstructRootAndSize()
        {
            var addr = 0;
            while (addr < _dataFileStream.Length)
            {
                var record = GetRecordAt(addr);

                if (record.IsValid)
                {
                    if (record.Data.IsRoot())
                        Root = addr;

                    Size += record.Data.GetDataCount();
                }

                addr += _recordSize;
            }
        }
    }
}