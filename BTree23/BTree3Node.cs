﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HeapFile;

namespace BTree3
{
    public class BTree3Node<T> : IRecord, IComparable<BTree3Node<T>>
        where T : IRecord, IComparable<T>, new()
    {
        public const int NH_SON = 0;
        public const int NH_DATA = 0;

        public const int N2_L_SON = 0;
        public const int N2_R_SON = 1;

        public const int N3_L_SON = 0;
        public const int N3_M_SON = 1;
        public const int N3_R_SON = 2;

        public const int N4_L_SON = 0;
        public const int N4_ML_SON = 1;
        public const int N4_MR_SON = 2;
        public const int N4_R_SON = 3;

        public const int N2_DATA = 0;

        public const int N3_L_DATA = 0;
        public const int N3_R_DATA = 1;

        public const int N4_L_DATA = 0;
        public const int N4_M_DATA = 1;
        public const int N4_R_DATA = 2;

        public const int FROM_LEFT = 0;
        public const int FROM_RIGHT = 1;

        public const long NOT_FOUND = -1;

        private long _parent;
        private List<T> _data;
        private List<long> _children;

        public BTree3Node()
        {
        }

        public BTree3Node(T data)
        {
            _data = new List<T>(3);
            _children = new List<long>(4);
            _data.Add(data);
        }

        public int CompareTo(BTree3Node<T> other)
        {
            return GetRightData().CompareTo(other.GetLeftData());
        }

        public int CompareTo(object obj)
        {
            if (obj is null) return 1;
            if (ReferenceEquals(this, obj)) return 0;
            return obj is BTree3Node<T> other
                ? CompareTo(other)
                : throw new ArgumentException($"Object must be of type {nameof(BTree3Node<T>)}");
        }

        public bool IsRoot()
        {
            return _parent == NOT_FOUND;
        }

        public bool IsLeaf()
        {
            return Degree() == 0;
        }

        public bool IsHole()
        {
            return _data.Count == 0;
        }

        public bool Is2Node()
        {
            return _data.Count == 1;
        }

        public bool Is3Node()
        {
            return _data.Count == 2;
        }

        public bool Is4Node()
        {
            return _data.Count == 3;
        }

        public int Degree()
        {
            return _children.Count;
        }

        public long GetParent()
        {
            return _parent;
        }

        public void SetParent(long newParent)
        {
            _parent = newParent;
        }

        public long GetBrother(BTree3Node<T> parent, long thisAddress, out int brotherFrom)
        {
            if (GetParent() == NOT_FOUND)
                throw new InvalidOperationException("Root does not have brother!");

            if (thisAddress == parent.GetLeftSon())
            {
                brotherFrom = FROM_RIGHT;
                return parent.Is3Node() ? parent.GetMiddleSon() : parent.GetRightSon();
            }

            if (thisAddress == parent.GetRightSon())
            {
                brotherFrom = FROM_LEFT;
                return parent.Is3Node() ? parent.GetMiddleSon() : parent.GetLeftSon();
            }

            brotherFrom = FROM_LEFT;
            return parent.GetLeftSon();
        }

        public long GetSonAt(int sonIndex)
        {
            return _children[sonIndex];
        }

        public void InsertLeftSon(long sonAddress)
        {
            InsertSonAt(sonAddress, N2_L_SON);
        }

        public void InsertRightSon(long sonAddress)
        {
            InsertSonAt(sonAddress, _children.Count);
        }

        public void InsertMiddleSon(long sonAddress)
        {
            InsertSonAt(sonAddress, N3_M_SON);
        }

        public void InsertSonAt(long sonAddress, int index)
        {
            _children.Insert(index, sonAddress);
        }

        public void ResetParent()
        {
            _parent = NOT_FOUND;
        }

        public int RemoveSon(BTree3Node<T> sonNode, long sonNodeAddress)
        {
            sonNode.ResetParent();

            for (var i = 0; i < _children.Count; i++)
            {
                if (_children[i] == sonNodeAddress)
                {
                    _children.RemoveAt(i);
                    return i;
                }
            }

            return (int) NOT_FOUND;
        }

        public override string ToString()
        {
            if (IsHole())
                return $"H[ / | / ]";

            if (Is2Node())
                return
                    $"2[ {GetDataAt(N2_DATA).ToString()} | / {{{(Degree() != 0 ? _children[0] + " " + _children[1] : "") + " ^" + _parent}}}]";

            if (Is3Node())
                return
                    $"3[ {GetDataAt(N3_L_DATA).ToString()} | {GetDataAt(N3_R_DATA).ToString()} {{{(Degree() != 0 ? _children[0] + " " + _children[1] + " " + _children[2] : "") + " ^" + _parent}}}]";

            return $"4[ X | X | X ]";
        }

        public T GetDataAt(int index)
        {
            return _data[index];
        }

        public bool ContainsData(T data)
        {
            if (Is2Node())
            {
                return data.CompareTo(_data[N2_DATA]) == 0;
            }

            if (Is3Node())
            {
                return data.CompareTo(_data[N3_L_DATA]) == 0 || data.CompareTo(_data[N3_R_DATA]) == 0;
            }

            return false;
        }

        public bool HasMiddleSon()
        {
            return Degree() == 3;
        }

        public bool HasMiddleLeftSon()
        {
            return HasMiddleSons();
        }

        public bool HasMiddleRightSon()
        {
            return HasMiddleSons();
        }

        private bool HasMiddleSons()
        {
            return Degree() == 4;
        }

        public bool HasOneSon()
        {
            return IsHole() && Degree() != 0;
        }

        public long GetLeftSon()
        {
            return !IsLeaf() ? _children[NH_SON] : NOT_FOUND;
        }

        public long GetMiddleSon()
        {
            return HasMiddleSon() ? _children[N3_M_SON] : NOT_FOUND;
        }

        public long GetMiddleLeftSon()
        {
            return HasMiddleLeftSon() ? _children[N4_ML_SON] : NOT_FOUND;
        }

        public long GetMiddleRightSon()
        {
            return HasMiddleRightSon() ? _children[N4_MR_SON] : NOT_FOUND;
        }

        public long GetOneSon()
        {
            return HasOneSon() ? _children[NH_SON] : NOT_FOUND;
        }

        public long GetRightSon()
        {
            return _children.Count switch
            {
                1 => _children[NH_SON],
                2 => _children[N2_R_SON],
                3 => _children[N3_R_SON],
                4 => _children[N4_R_SON],
                _ => NOT_FOUND,
            };
        }

        public T GetRightData()
        {
            return _data.Count switch
            {
                1 => _data[N2_DATA],
                2 => _data[N3_R_DATA],
                3 => _data[N4_R_DATA],
                _ => throw new InvalidOperationException("I have no data!"),
            };
        }
        public T GetLeftData()
        {
            return !IsHole() ? _data[N2_DATA] : throw new InvalidOperationException("I have no data!");
        }

        public T GetMiddleData()
        {
            return Is4Node()
                ? _data[N4_M_DATA]
                : throw new InvalidOperationException("I have no data in middle!");
        }

        public T GetOneData()
        {
            if (Is2Node())
                return _data[N2_DATA];

            throw new InvalidOperationException("I'm not 2-node ...");
        }

        public bool RemoveData(T data)
        {
            for (var i = 0; i < _data.Count; i++)
                if (_data[i].CompareTo(data) == 0)
                {
                    _data.RemoveAt(i);
                    return true;
                }

            return false;
        }

        public void InsertData(T data)
        {
            _data.Add(data);
            _data.Sort();
        }

        public T GetData(T data)
        {
            return _data.Count switch
            {
                1 => _data[N2_DATA],
                2 => _data[N3_L_DATA].CompareTo(data) == 0 ? _data[N3_L_DATA] : _data[N3_R_DATA],
                _ => default,
            };
        }


        public int GetDataCount()
        {
            return _data.Count;
        }

        public byte[] ToByteArray()
        {
            // parent
            var parentBytes = BitConverter.GetBytes(_parent);

            var dataCount = (byte) _data.Count;
            var validData =  new []{dataCount};

            // children
            var children = new byte[] { };
            for (var i = 0; i < 3; i++)
                if (i < _children.Count)
                    children = children.Concat(BitConverter.GetBytes(_children[i])).ToArray();
                else
                    children = children.Concat(BitConverter.GetBytes(NOT_FOUND)).ToArray(); // sizeof(long)

            // datas
            var datas = new byte[] { };
            var dataSize = new T().ByteSize();
            for (var i = 0; i < 2; i++)
                if (i < _data.Count)
                    datas = datas
                        .Concat(_data[i].ToByteArray())
                        .ToArray();
                else
                    datas = datas.Concat(new byte[dataSize]).ToArray();

            return parentBytes
                .Concat(validData)
                .Concat(children)
                .Concat(datas)
                .ToArray();
        }

        public void LoadFromByteArray(byte[] array)
        {
            // parent
            var parentBytes = array[..sizeof(long)];
            _parent = BitConverter.ToInt64(parentBytes);

            var validDataBytes = array[sizeof(long)..(sizeof(long) + sizeof(byte))];
            var validData = validDataBytes[0];

            // children
            _children = new List<long>(4);
            for (var i = 0; i < 3; i++)
            {
                var begin = sizeof(long) + sizeof(byte) + i * sizeof(long);
                var end = sizeof(long) + sizeof(byte) + (i + 1) * sizeof(long);
                var childBytes = array[begin .. end];
                var childAddr = BitConverter.ToInt64(childBytes);
                if (childAddr != NOT_FOUND)
                    _children.Add(childAddr);
            }

            var dataStart = sizeof(byte) + 4 * sizeof(long);

            // data
            _data = new List<T>(3);
            var dataSize = new T().ByteSize();
            for (var i = 0; i < validData; i++)
            {
                var begin = dataStart + i * dataSize;
                var end = dataStart + i * dataSize + dataSize;
                var dataBytes = array[begin .. end];
                var data = new T();
                data.LoadFromByteArray(dataBytes);
                _data.Add(data);
            }
        }

        public int ByteSize()
        {
            var parent = sizeof(long);
            var validData = sizeof(byte);
            var child = sizeof(long);
            var data = new T().ByteSize();

            return parent + validData + 3 * child + 2 * data;
        }
    }
}